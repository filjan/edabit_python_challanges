"""
https://edabit.com/challenge/KBmKcorkjbuXds6Jo
Chocolates Parcel
Mubashir needs to assemble a parcel of ordered chocolates.
He got two types of chocolates:

Small chocolates (2 grams each)
Big chocolates (5 grams each)
Create a function that takes three parameters:
    Number of small available chocolates n_small,
    number of big chocolates available n_big
    and desired weight (in grams) of the final parcel order.

The function should return the required number of small chocolates to achieve
the goal. The function should return -1 if the goal cannot be achieved by any
possible combinations of small and big chocolates.

Examples
chocolates_parcel(4, 1, 13) ➞ 4
# 4 small chocolates = 8 grams
# 1 big chocolate = 5 grams
# 8 + 5 = 13 grams
# Required number of small chocolates = 4

chocolates_parcel(4, 1, 14) ➞ -1
# You can not make any combination to reach 14 grams.

chocolates_parcel(2, 1, 7) ➞ 1
# 1 big chocolate = 5 grams
# 1 small chocolates = 2 grams
# 5 + 2 = 7 grams
# Required number of small chocolates = 1

Notes
Maximize the use of big chocolates that are available to achieve the desired
goal. And only then should you proceed to use the small chocolates.
You can't break chocolates into small pieces.
"""

# Constants - weights of single chocolates
WEIGHT_SMALL = 2
WEIGHT_BIG = 5

def chocolates_parcel(n_small, n_big, order):
    """
    Function chooses the best amount of small and big chocolated to fulfil
    requiremnt of ordered weight.
    Function maximizes number of big chocolates.
    """
    # Prospective maximum number of big chocolates in order
    max_n_of_big = min(n_big, order // WEIGHT_BIG)
    # Iteratively deacrease number of big chocolates in the order
    # Two conditions - number of big chocolates is at least 0, and rest
    # amount of small with chosen big must be equal or higher than order
    while (
        max_n_of_big >= 0
        and max_n_of_big * WEIGHT_BIG + n_small * WEIGHT_SMALL >= order
    ):
        # Rest weight of ordered required to fulfil with small chocolates
        rest_weight = order - max_n_of_big * WEIGHT_BIG
        # Res weight should be fulfiled evenly with no more than
        # available amount of small chocolates
        prospective_n_of_small = rest_weight // WEIGHT_SMALL
        if not rest_weight % WEIGHT_SMALL and prospective_n_of_small <= n_small:
            return prospective_n_of_small
        # If requirement is not fulfiled deacrase number of big by one piece
        max_n_of_big -= 1

    return -1


if __name__ == "__main__":

    from time import perf_counter
    import numpy as np

    tic = perf_counter()

    np.testing.assert_equal(chocolates_parcel(0, 1, 5), 0)
    np.testing.assert_equal(chocolates_parcel(3, 1, 6), 3)
    np.testing.assert_equal(chocolates_parcel(3, 0, 7), -1)
    np.testing.assert_equal(chocolates_parcel(2, 1, 9), 2)
    np.testing.assert_equal(chocolates_parcel(58, 156, 283), 4)
    np.testing.assert_equal(chocolates_parcel(3, 1000, 5012), -1)
    np.testing.assert_equal(chocolates_parcel(1, 1, 1), -1)
    np.testing.assert_equal(chocolates_parcel(1, 1, 8), -1)
    np.testing.assert_equal(chocolates_parcel(4, 1, 12), -1)
    np.testing.assert_equal(chocolates_parcel(10, 400, 2004), 2)

    print(f"t_sec={perf_counter()-tic:.6f}")