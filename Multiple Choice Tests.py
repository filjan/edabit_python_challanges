"""
https://edabit.com/challenge/Ld4xBpqBXqygwQ5St

Multiple Choice Tests
Your task is to write a program which allows teachers to create a multiple
choice test in a class called Testpaper and to be also able to assign a
minimum pass mark. The testpaper's subject should also be included.
The attributes are in the following order:

subject
markscheme
pass_mark
As well as that, we need to create student objects to take the test itself!
Create another class called Student and do the following:

Create an attribute called tests_taken and set the default as 'No tests taken'.
Make a method called take_test(), which takes in the testpaper object they are
completing and the student's answers. Compare what they wrote to the mark
scheme, and append to the/create a dictionary assigned to tests_taken in
the way as shown in the point below.
Each key in the dictionary should be the testpaper subject and each value
should be a string in the format seen in the examples below (whether or not
the student has failed, and their percentage in brackets).

Examples
paper1 = Testpaper("Maths", ["1A", "2C", "3D", "4A", "5A"], "60%")
paper2 = Testpaper("Chemistry", ["1C", "2C", "3D", "4A"], "75%")
paper3 = Testpaper("Computing", ["1D", "2C", "3C", "4B", "5D", "6C", "7A"], "75%")

student1 = Student()
student2 = Student()
student1.tests_taken ➞ "No tests taken"
student1.take_test(paper1, ["1A", "2D", "3D", "4A", "5A"])
student1.tests_taken ➞ {"Maths" : "Passed! (80%)"}

student2.take_test(paper2, ["1C", "2D", "3A", "4C"])
student2.take_test(paper3, ["1A", "2C", "3A", "4C", "5D", "6C", "7B"])
student2.tests_taken ➞ {"Chemistry" : "Failed! (25%)", "Computing" : "Failed! (43%)"}
Notes
Round percentages to the nearest whole number.
Remember that the attribute tests_taken should return 'No tests taken' when
no tests have been taken yet.
"""

from dataclasses import dataclass
from typing import Text, List


@dataclass
class Testpaper:
    """ Testpapaer class """

    subject: Text
    # List of strings describing correct marks
    markscheme: List
    # Passing percentage of correct marks
    pass_mark: Text


class StudentDecorator:
    """ Student class decorators """
    @staticmethod
    def check_input_correctness(fun):
        """ Decorator handle checking the correctness of input data """

        def wrapper(self, testpaper: Testpaper, marks: List):
            """ Wrapper check correctness of input values """
            # Mandatory condition: Number of done exams must be equal to
            # markscheme
            if len(testpaper.markscheme) != len(marks):
                raise Exception("Marskchemes length doesn't correspond to marks")
            # Mandatory condition: Each descriptive number must exists in
            # each list
            zipped_marks = zip(marks, testpaper.markscheme)
            if not all(list(map(lambda x: x[0][0] == x[1][0], zipped_marks))):
                raise Exception("Marskchemes amount doesn't correspond with mark")
            fun(self, testpaper, marks)
            #

        return wrapper

    @staticmethod
    def prepare_attribute(fun):
        """ Decorator handles test_taken attribute """

        def wrapper(self, testpaper: Testpaper, marks: List):
            # Declare tests_taken as dictionary when take_test calling
            if self.tests_taken == "No tests taken":
                self.tests_taken = {}
            # Call take_test function
            function_response = fun(self, testpaper, marks)
            # Assign students result on subject into test_taken
            self.tests_taken[testpaper.subject] = function_response

        return wrapper


class Student(StudentDecorator):
    """ Student class """

    def __init__(self):
        """ Initialization of Student """
        self.tests_taken = "No tests taken"

    @StudentDecorator.check_input_correctness
    @StudentDecorator.prepare_attribute
    def take_test(self, testpaper: Testpaper, marks: List) -> Text:  # pylint: disable=R0201
        """
        take_test check what percentage of marks are correct from testpaper
        """
        # Check how many students marks are correct
        zipped_marks = zip(marks, testpaper.markscheme)
        passed_marks = tuple(map(lambda x: x[0][-1] == x[1][-1], zipped_marks))
        number_of_passed_marks = passed_marks.count(True)
        # Convert students results from quotient into percentage
        students_level = round(100 * number_of_passed_marks / len(marks))
        # Compare students result with pass criterion
        if students_level >= int(testpaper.pass_mark[:-1]):
            grade_note = f"Passed! ({students_level}%)"
        else:
            grade_note = f"Failed! ({students_level}%)"
        #
        return grade_note


if __name__ == "__main__":

    import numpy as np

    paper1 = Testpaper("Maths", ["1A", "2C", "3D", "4A", "5A"], "60%")
    paper2 = Testpaper("Chemistry", ["1C", "2C", "3D", "4A"], "75%")
    paper3 = Testpaper("Computing", ["1D", "2C", "3C", "4B", "5D", "6C", "7A"], "75%")
    paper4 = Testpaper(
        "Physics",
        ["1A", "2B", "3A", "4C", "5A", "6C", "7A", "8C", "9D", "10A", "11A"],
        "90%",
    )

    student1 = Student()
    student2 = Student()
    student3 = Student()

    np.testing.assert_equal(student1.tests_taken, "No tests taken")
    student1.take_test(paper1, ["1A", "2D", "3D", "4A", "5A"])
    np.testing.assert_equal(student1.tests_taken, {"Maths": "Passed! (80%)"})

    student2.take_test(paper2, ["1C", "2D", "3A", "4C"])
    student2.take_test(paper3, ["1A", "2C", "3A", "4C", "5D", "6C", "7B"])
    np.testing.assert_equal(
        student2.tests_taken,
        {"Chemistry": "Failed! (25%)", "Computing": "Failed! (43%)"},
    )

    np.testing.assert_equal(student3.tests_taken, "No tests taken")
    student3.take_test(paper1, ["1C", "2D", "3A", "4C", "5A"])
    student3.take_test(paper3, ["1A", "2C", "3A", "4C", "5D", "6C", "7B"])
    np.testing.assert_equal(
        student3.tests_taken,
        {"Maths": "Failed! (20%)", "Computing": "Failed! (43%)"},
    )
    student3.take_test(
        paper4,
        ["1A", "2C", "3A", "4C", "5D", "6C", "7B", "8C", "9D", "10A", "11A"],
    )
    np.testing.assert_equal(
        student3.tests_taken,
        {
            "Maths": "Failed! (20%)",
            "Computing": "Failed! (43%)",
            "Physics": "Failed! (73%)",
        },
    )

    np.testing.assert_equal(paper1.subject, "Maths")
    np.testing.assert_equal(paper2.subject, "Chemistry")
    np.testing.assert_equal(paper3.subject, "Computing")
    np.testing.assert_equal(paper4.subject, "Physics")

    np.testing.assert_equal(paper1.markscheme, ["1A", "2C", "3D", "4A", "5A"])
    np.testing.assert_equal(paper2.markscheme, ["1C", "2C", "3D", "4A"])
    np.testing.assert_equal(paper3.markscheme, ["1D", "2C", "3C", "4B", "5D", "6C", "7A"])
    np.testing.assert_equal(
        paper4.markscheme,
        ["1A", "2B", "3A", "4C", "5A", "6C", "7A", "8C", "9D", "10A", "11A"],
    )

    np.testing.assert_equal(paper1.pass_mark, "60%")
    np.testing.assert_equal(paper2.pass_mark, "75%")
    np.testing.assert_equal(paper3.pass_mark, "75%")
    np.testing.assert_equal(paper4.pass_mark, "90%")