"""
https://edabit.com/challenge/WD5afSjoPDub5bc6J

Padovan Sequence
In number theory, the Padovan sequence is the sequence of integers P(n)
defined by the initial values:

P(0) = P(1) = P(2) = 1
And the recurrence relation:

P(n) = P(n-2) + P(n-3)
As with any sequence defined by a recurrence relation, Padovan numbers P(m)
for m<0 can be defined by rewriting the recurrence relation as:

P(m) = P(m+3) - P(m+1)
Objective
Create a function that takes two numbers, m and n, being m always negative
and n always positive, and returns a list with the Padovan numbers between
P(m) and P(n).

Examples
padovan(-1, 1) ➞ [0, 1, 1])

padovan(-10, 10) ➞ [2, -1, 0, 1, -1, 1, 0, 0, 1, 0, 1, 1, 1, 2, 2, 3, 4, 5
                    7, 9, 12]

padovan(-50, 1) ➞ [-524, 245, 71, -279, 316, -208, 37, 108, -171, 145, -63,
                    -26, 82, -89, 56, -7, -33, 49, -40, 16, 9, -24, 25, -15,
                    1, 10, -14, 11, -4, -3, 7, -7, 4, 0, -3, 4, -3, 1, 1, -2,
                    2, -1, 0, 1, -1, 1, 0, 0, 1, 0, 1, 1]
Notes
N/A
"""


def build_padovan(n, p):
    """
    Function returns value proper value for n index in Padovan sequence
    """
    # Values are equal 1
    if n in (0,1,2):
        val_return = 1
    # Above 2 acc. to difference below
    elif n > 2:
        val_return = p[n - 2] + p[n - 3]
    # Negative n acc. to difference below with exception that viewing from
    # negative might occur to be out of the list - in positive index, and it
    # might not exist yet -> assume this value as 1.
    else:   # i.e. n < 0:
        try:
            val_1 = p[n + 3]
        except IndexError:
            val_1 = 1
        try:
            val_2 = p[n + 1]
        except IndexError:
            val_2 = 1
        val_return = val_1 - val_2

    return val_return


def padovan(min_val, max_val):
    """
    Function returns list of padovan sequence  with indices acc. to given range.
    """
    # Define padovan's sequence indices
    range_neg = list(range(-1, min_val - 1, -1))
    range_pos = list(range(0, max_val + 1))
    # Fill positive (+0) indices with padovan sequence
    p_pos = []
    for pos in range_pos:
        p_pos.append(build_padovan(pos, p_pos))
    # Fill negative indices with padovan sequence
    p_neg = []
    for neg in range_neg:
        p_neg.insert(0, build_padovan(neg, p_neg))
    # Join p_pos and p_neg together and return
    return p_neg + p_pos


if __name__ == "__main__":
    a = padovan(-50, 1)

    import numpy as np

    np.testing.assert_equal(
        padovan(-10, 10),
        [2, -1, 0, 1, -1, 1, 0, 0, 1, 0, 1, 1, 1, 2, 2, 3, 4, 5, 7, 9, 12],
    )
    np.testing.assert_equal(
        padovan(-1, 100),
        [
            0,
            1,
            1,
            1,
            2,
            2,
            3,
            4,
            5,
            7,
            9,
            12,
            16,
            21,
            28,
            37,
            49,
            65,
            86,
            114,
            151,
            200,
            265,
            351,
            465,
            616,
            816,
            1081,
            1432,
            1897,
            2513,
            3329,
            4410,
            5842,
            7739,
            10252,
            13581,
            17991,
            23833,
            31572,
            41824,
            55405,
            73396,
            97229,
            128801,
            170625,
            226030,
            299426,
            396655,
            525456,
            696081,
            922111,
            1221537,
            1618192,
            2143648,
            2839729,
            3761840,
            4983377,
            6601569,
            8745217,
            11584946,
            15346786,
            20330163,
            26931732,
            35676949,
            47261895,
            62608681,
            82938844,
            109870576,
            145547525,
            192809420,
            255418101,
            338356945,
            448227521,
            593775046,
            786584466,
            1042002567,
            1380359512,
            1828587033,
            2422362079,
            3208946545,
            4250949112,
            5631308624,
            7459895657,
            9882257736,
            13091204281,
            17342153393,
            22973462017,
            30433357674,
            40315615410,
            53406819691,
            70748973084,
            93722435101,
            124155792775,
            164471408185,
            217878227876,
            288627200960,
            382349636061,
            506505428836,
            670976837021,
            888855064897,
            1177482265857,
        ],
    )
    np.testing.assert_equal(
        padovan(-50, 1),
        [
            -524,
            245,
            71,
            -279,
            316,
            -208,
            37,
            108,
            -171,
            145,
            -63,
            -26,
            82,
            -89,
            56,
            -7,
            -33,
            49,
            -40,
            16,
            9,
            -24,
            25,
            -15,
            1,
            10,
            -14,
            11,
            -4,
            -3,
            7,
            -7,
            4,
            0,
            -3,
            4,
            -3,
            1,
            1,
            -2,
            2,
            -1,
            0,
            1,
            -1,
            1,
            0,
            0,
            1,
            0,
            1,
            1,
        ],
    )
    np.testing.assert_equal(
        padovan(-3, 33),
        [
            0,
            1,
            0,
            1,
            1,
            1,
            2,
            2,
            3,
            4,
            5,
            7,
            9,
            12,
            16,
            21,
            28,
            37,
            49,
            65,
            86,
            114,
            151,
            200,
            265,
            351,
            465,
            616,
            816,
            1081,
            1432,
            1897,
            2513,
            3329,
            4410,
            5842,
            7739,
        ],
    )
    np.testing.assert_equal(padovan(-1, 1), [0, 1, 1])
    np.testing.assert_equal(padovan(-9, 2), [-1, 0, 1, -1, 1, 0, 0, 1, 0, 1, 1, 1])
