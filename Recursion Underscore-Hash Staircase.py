"""
https://edabit.com/challenge/ntpgCFga2rRzB53QZ

Recursion: Underscore-Hash Staircase
Create a function that will build a staircase using the underscore _ and hash
# symbols. A positive value denotes the staircase's upward direction and
downwards for a negative value.

Examples
staircase(3) ➞ "__#\n_##\n###"
__#
_##
###

staircase(7) ➞ "______#\n_____##\n____###\n___####\n__#####\n_######\n#######"
______#
_____##
____###
___####
__#####
_######
#######

staircase(2) ➞ "_#\n##"
_#
##

staircase(-8) ➞ "########\n_#######\n__######\n___#####\n____####\n_____###\n______##\n_______#"
########
_#######
__######
___#####
____####
_____###
______##
_______#
Notes
All inputs are either positive or negative values.
The string to be returned should be adjoined with the newline character \n.
You're expected to solve this challenge using a recursive approach.
You can read more on recursion (see Resources tab) if you aren't familiar with
it or haven't fully understood the concept before taking this challenge.
A non-recursive version of this challenge can be found here.
"""


def update_n(n):
    """
    Function:
        - decreases n by 1 when its positive
        or
        - increase n by 1 when negative
    """
    if n < 0:
        n += 1
    elif n > 0:
        n -= 1
    else:
        raise Exception("n can't be 0 during calling update_n")
    return n


def define_num_of_sharps_unders(n):
    """
    Function returns tuple of two string: underscors and sharps. Each has
    proper length to join them into one string (as row)
    """
    # Determine width of the staircase's board
    if n < 0:
        width = len(__stairs[0])
    elif n > 0:
        width = len(__stairs[-1])
    # Number of sharps, underscores is independent to sign in front of n
    sharps = (width - abs(n)) * "_"
    unders = abs(n) * "#"
    #
    return sharps, unders


def append_to_stairs(n, new_line_characters):
    """
    Function joins undercores and sharps into one string and append it to
    __stairs list, in proper way:
        - if n is negative: join string into end of list
        - if n is positive: join string into beginning of the list
    """
    new_line_string = "".join(new_line_characters)
    if n < 0:
        __stairs.append(new_line_string)
    elif n > 0:
        __stairs.insert(0, new_line_string)


def staircase(n):
    """
    Function create hidden variable __stairs and revursively fill it properly
    with underscores and sharps to create starcase according to argument n.
    Just before starcase string is returned, hidden variable value had been
    deleted
    """
    # Define stairs as global (as argument is forbidden) then - fill it with
    # initial row of '#'
    if "__stairs" not in globals().keys():
        global __stairs
        __stairs = [abs(n) * "#"]
        n = update_n(n)
    # Finish recurency condition - when n equals 0
    if n == 0:
        stairs_return = "\n".join(__stairs)
        del __stairs
        return stairs_return
    # Define number of sharps and underscores acc. to current n and starcase
    # width
    new_line_characters = define_num_of_sharps_unders(n)
    # Append n-characters into __stairs variables
    append_to_stairs(n, new_line_characters)
    # Update n (increase or decrease by one)
    n = update_n(n)
    # call starcase recursevily
    return staircase(n)


if __name__ == "__main__":
    a = staircase(1)
    print(f"ret : \n{a}")

    b = staircase(-2)
    print(f"ret : \n{b}")

    c = staircase(3)
    print(f"ret : \n{c}")

    ## Edabit tests # I've changed "> 1" to "> 2" because it doesn't make sense.
    # In the recursive way must exist two names in whole function - the name,
    # and return

    from inspect import getsource
    from re import findall, MULTILINE

    import numpy as np

    def check_recursive(fn):
        try:
            src, n = getsource(fn), fn.__name__
            if n == "<lambda>":
                n = src.split("=")[0].strip()
            return len(findall(n, src, flags=MULTILINE)) > 2
        except OSError:
            return True

    np.testing.assert_equal(check_recursive(staircase), False, "Recursion is required!")

    num_vector = [3, 7, 2, -8, 4, -12, 11, -6]
    res_vector = [
        "__#\n_##\n###",
        "______#\n_____##\n____###\n___####\n__#####\n_######\n#######",
        "_#\n##",
        "########\n_#######\n__######\n___#####\n____####\n_____###\n______##\n_______#",
        "___#\n__##\n_###\n####",
        "############\n_###########\n__##########\n___#########\n____########\n_____#######\n______######\n_______#####\n________####\n_________###\n__________##\n___________#",
        "__________#\n_________##\n________###\n_______####\n______#####\n_____######\n____#######\n___########\n__#########\n_##########\n###########",
        "######\n_#####\n__####\n___###\n____##\n_____#",
    ]
    for i, x in enumerate(num_vector):
        np.testing.assert_equal(staircase(x), res_vector[i])