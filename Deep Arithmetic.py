"""
https://edabit.com/challenge/FgAsxMCaEzvKhnuAH

Deep Arithmetic
Create a function that takes an array of strings of arbitrary dimensionality
([], [][], [][][], etc) and returns the deep_sum of every separate number in
each string in the array.

Examples
deep_sum(["1",  "five",  "2wenty",  "thr33"]) ➞ 36

deep_sum([["1X2",  "t3n"], ["1024", "5", "64"]]) ➞ 1099

deep_sum([[["1"], "10v3"],  ["738h"],  [["s0"],  ["1mu4ch3"], "-1s0"]]) ➞ 759
Notes
Numbers in strings can be negative, but will all be base-10 integers.
Negative numbers may directly follow another number.
The hyphen or minus character ("-") does not only occur in numbers.
Arrays may be ragged or empty.
"""

import re
from copy import deepcopy

def deep_sum(x):
    """
    Function returns result of addition each positive and negative figure
    hidden in nested strings inside lists.
    """
    # Work further on deep copy of mutable given argument(list here)
    _x = deepcopy(x)
    # Flatten x as deep as it's not include any list
    while any(map(lambda comp: isinstance(comp, list), _x)):
        value_to_unpack = list(filter(lambda val: isinstance(val, list), _x))[0]
        _x.extend(value_to_unpack)
        _x.remove(value_to_unpack)
    # If _x es empty just return 0
    if not _x:
        return 0
    # Join each substring with +
    long_string = "+".join(_x)
    # Replace each alphanumeric character with +
    long_string = re.sub("[A-Z,a-z]", "+", long_string)
    # Replace each minus with +-0 - its neutral to negative meaning o values,
    # but splits each figure by +
    long_string = re.sub("-", "+-0", long_string)
    # Split string by + sign, and rid of each empty component
    numbers_as_strings = list(filter(lambda comp: comp != "", long_string.split("+")))
    # Its necesary to map each component into int to rid of 09, 03 etc.
    numbers = list(map(lambda comp: str(int(comp)), numbers_as_strings))
    # Again merge each component with + sign
    numbers_as_sum = "+".join(numbers)
    # Eval string as addition
    result = eval(numbers_as_sum)
    #
    return result


if __name__ == "__main__":
    print(deep_sum(["1", "five", "2wenty", "thr33"]))
    print(deep_sum([["1X2", "t3n"], ["1024", "5", "64"]]))
    print(deep_sum([[["1"], "10v3"], ["738h"], [["s0"], ["1mu4ch3"], "-1s0"]]))

    import numpy as np

    ### Edabit tests
    actual_param = [
        ["1", "five", "2wenty", "thr33"],
        [["1X2", "t3n"], ["1024", "5", "64"]],
        [[["1"], "10v3"], ["738h"], [["s0"], ["1mu4ch3"], "-1s0"]],
        [
            [["0", "0x2", "z3r1"], ["1", "55a46"]],
            [["1", "0b2", "4"], ["0x5fp-2", "nine", "09"], ["4", "4", "4"]],
            [["03"]],
            [],
        ],
        [[[[[[[[[[[[[[[["-1", "1"], ["3"], [""], []]]]]]]]]]]]]]]],
        [[[[[[[[[[[[[[[[]]]]]]]]]]]]]]]],
        [[[[[["-32-64", "a-zA-Z"], ["01-1"]]]]]],
    ]
    expected_param = [36, 1099, 759, 142, 3, 0, -96]
    for i, x in enumerate(actual_param):
        np.testing.assert_equal(deep_sum(x), expected_param[i])