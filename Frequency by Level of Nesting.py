"""
https://edabit.com/challenge/Yp8crKmgxZ3HiSBAZ

Frequency by Level of Nesting
Create a function that takes in a nested list and an element and returns the
frequency of that element by nested level.

Examples
freq_count([1, 4, 4, [1, 1, [1, 2, 1, 1]]], 1)
➞ [[0, 1], [1, 2], [2, 3]]
# The list has one 1 at level 0, 2 1's at level 1, and 3 1's at level 2.

freq_count([1, 5, 5, [5, [1, 2, 1, 1], 5, 5], 5, [5]], 5)
➞ [[0, 3], [1, 4], [2, 0]]

freq_count([1, [2], 1, [[2]], 1, [[[2]]], 1, [[[[2]]]]], 2)
➞ [[0, 0], [1, 1], [2, 1], [3, 1], [4, 1]]
Notes
Start the default nesting (a list with no nesting) at 0.
Output the nested levels in order (e.g. 0 first, then 1, then 2, etc).
Output 0 for the frequency if that particular level has no instances of that
element (see example #2).
"""


def freq_count(lst, el):
    """
    Function returns number of values in each nest level
    """
    # Declare result container
    count_list = []
    # initialize nesting counter
    iteration = 0
    # Iterate while lst is not empty
    while lst:
        # Count how many occurences of el is in lst
        count_list.append([iteration, lst.count(el)])
        # Extract nested lists from lst
        lst = list(filter(lambda x: isinstance(x, list), lst))
        # Flatten most shallows lists
        lst = [item for sublist in lst for item in sublist]
        # Increase nested level to be
        iteration += 1
    return count_list


if __name__ == "__main__":
    print(freq_count([1, 4, 4, [1, 1, [1, 2, 1, 1]]], 1)) # [[0, 1], [1, 2], [2, 3]]

    print(freq_count([1, 5, 5, [5, [1, 2, 1, 1], 5, 5], 5, [5]], 5)) # ➞ [[0, 3], [1, 4], [2, 0]]

    import numpy as np

    np.testing.assert_equal(freq_count([1, 1, 1, 1], 1), [[0, 4]])
    np.testing.assert_equal(freq_count([1, 1, 2, 2], 1), [[0, 2]])
    np.testing.assert_equal(freq_count([1, 1, 2, [1]], 1), [[0, 2], [1, 1]])
    np.testing.assert_equal(freq_count([1, 1, 2, [[1]]], 1), [[0, 2], [1, 0], [2, 1]])
    np.testing.assert_equal(freq_count([[[1]]], 1), [[0, 0], [1, 0], [2, 1]])
    np.testing.assert_equal(
        freq_count([1, 4, 4, [1, 1, [1, 2, 1, 1]]], 1), [[0, 1], [1, 2], [2, 3]]
    )
    np.testing.assert_equal(
        freq_count([1, 5, 5, [5, [1, 2, 1, 1], 5, 5], 5, [5]], 5),
        [[0, 3], [1, 4], [2, 0]],
    )
    np.testing.assert_equal(
        freq_count([1, [2], 1, [[2]], 1, [[[2]]], 1, [[[[2]]]]], 2),
        [[0, 0], [1, 1], [2, 1], [3, 1], [4, 1]],
    )