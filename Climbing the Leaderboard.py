"""
https://edabit.com/challenge/NmZaXN5BH7miSAz6s

Climbing the Leaderboard
An arcade game player wants to climb to the top of the leaderboard and track
their ranking. The game uses Dense Ranking, so its leaderboard works like this:

The player with the highest score is ranked number 1 on the leaderboard.
Players who have equal scores receive the same ranking number, and the next
player(s) receive the immediately following ranking number.
Create a function that takes two lists of integers.

ranked[[100, 90, 90, 80]]
player[[70, 80, 105]]
The ranked players will have ranks 1, 2, 2, and 3, respectively. If the
player's scores are 70, 80 and 105, their rankings after each game are 4th,
3rd , and 1st. Return [[4,3,1]].

Explanation
climbing_leaderboard([100, 100, 50, 40, 40, 20, 10], [5, 25, 50, 120])
➞ [6, 4, 2, 1]
Alice starts playing with 7 players already on the leaderboard, which looks
like this: snapshot1

After Alice finishes game 0, her score is 5 and her ranking is 6: snapshot2
[pic.]
After Alice finishes game 1, her score is 25 and her ranking is 4: snapshot3
[pic.]
After Alice finishes game 2, her score is 50 and her ranking is tied with
Caroline at 2: snapshot4
[pic.]
After Alice finishes game 3, her score is 120 and her ranking is 1: snapshot5
[pic.]

Examples
climbing_leaderboard([100, 90, 90, 80, 75, 60], [50, 65, 77, 90, 102])
➞ [6, 5, 4, 2, 1]

climbing_leaderboard([80, 80, 80, 75, 70, 60, 60, 60], [70, 72, 78, 88])
➞ [3, 3, 2, 1]
Notes
N/A
"""


class Table:
    """ Table class with rank of all players """
    def __init__(self, ranked, player):
        # List of input players points
        self.ranked = ranked
        # Extended overiew over players and points. Initially each player has
        # rank 0
        self.overview = [
            {"player_id": player_id, "points": points, "rank": 0}
            for player_id, points in enumerate(self.ranked)
        ]
        # Additional players points after each consecutive game
        self.player = player
        # Find current maximum player id, increase it by one
        self.additional_player_id = (
            max(self.overview, key=lambda x: x["player_id"])["player_id"] + 1
        )
        # Define instance of new player
        self.additional_player = {
            "player_id": self.additional_player_id,
            "points": 0,
            "rank": 0,
        }

    def process_rank(self):
        """ Function updates rank positions in overview variable """
        # Initialy sort each player n overview by points in descend order
        self.overview.sort(key=lambda x: x["points"], reverse=True)
        # # Extract list of ordered points of each player
        points = [player["points"] for player in self.overview]
        # Define initial value of rank position (incrementally changed value)
        rank_position = 1
        # Iterate over list of points of each player
        for ind, (player_points, player_points_next) in enumerate(
            zip(points, points[1:] + [points[-1]])
        ):
            # Enter rank position to specific player row
            self.overview[ind]["rank"] = rank_position
            # if points now are higher than in next iteration, it means that
            # next player has lower rank position
            if player_points > player_points_next:
                rank_position += 1

    def play_with_additional_player(self):
        """ Add new player with points defined in 'player' argument """
        # Append new player to the overview variable
        self.overview.append(self.additional_player)
        # Iterate over points of the new player after each game
        for additonal_player_points_after_game in self.player:
            # Update number of points
            self.additional_player["points"] = additonal_player_points_after_game
            # Process rank iteratively
            self.process_rank()
            # Yield rank position of the new player
            yield self.additional_player["rank"]


def climbing_leaderboard(ranked, player):
    """
    Function service Table class, and returns consecutive rank positions
    of the new player after each game
    """
    games_table = Table(ranked, player)
    consecutive_rank_positions = list(games_table.play_with_additional_player())
    return consecutive_rank_positions


if __name__ == "__main__":

    import numpy as np

    np.testing.assert_equal(
        climbing_leaderboard([100, 100, 50, 40, 40, 20, 10], [5, 25, 50, 120]),
        [6, 4, 2, 1],
    )
    np.testing.assert_equal(
        climbing_leaderboard([100, 90, 90, 80, 75, 60], [50, 65, 77, 90, 102]),
        [6, 5, 4, 2, 1],
    )
    np.testing.assert_equal(
        climbing_leaderboard([80, 80, 80, 75, 70, 60, 60, 60], [70, 72, 78, 88]),
        [3, 3, 2, 1],
    )
    np.testing.assert_equal(
        climbing_leaderboard([120, 99, 95, 90, 89, 70, 60, 60, 50, 50], [65, 90, 150]),
        [7, 4, 1],
    )
    np.testing.assert_equal(
        climbing_leaderboard([500, 400, 300, 200, 150, 50], [40, 90, 150]), [7, 6, 5]
    )