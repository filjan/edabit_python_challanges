"""
https://edabit.com/challenge/ZwmfET5azpvBTWoQT
Word Nests (Part 2)
A word nest is created by taking a starting word, and generating a new string
by placing the word inside itself. This process is then repeated.

Nesting 3 times with the word "incredible":

start  = incredible
first  = incre(incredible)dible
second = increin(incredible)credibledible
third  = increinincr(incredible)ediblecredibledible
The final nest is increinincrincredibleediblecredibledible (depth = 3)

Valid word nests can always be collapsed to show the starting word, by reversing
the process above:

word = "incredible"
nest = "increinincrincredibleediblecredibledible"

Steps:
=> "increinincrincredibleediblecredibledible" # starting nest
=> "increinincr(incredible)ediblecredibledible" # find word in nest
=> "increinincr            ediblecredibledible" # remove word
=> "increinincrediblecredibledible" # join remaining halves
=> "increin(incredible)credibledible" # find word in nest...

... repeat steps until single word remains

=> "incredible" (return True as "incredible" = word)
When invalid word nests are collapsed, the starting word isn't found:

word = "spring"
nest = "sprspspspringringringg"

Steps:
=> "sprspspspringringringg" # starting nest
=> "sprspsp(spring)ringringg" # find word in nest
=> "sprspsp        ringringg" # remove word
=> "sprspspringringg" # join remaining halves
=> "sprsp(spring)ringg" # find word in nest...

... repeat steps until single word remains

=> "sprg" (return False as "sprig" != "spring")
Given a starting word and a final word nest, return True if the word nest is
valid. Return False otherwise.

Examples
valid_word_nest("deep", "deep") ➞ True

valid_word_nest("novel", "nonnonovnovnovelelelvelovelvel") ➞ True

valid_word_nest("painter", "ppaintppapaipainterinternteraintererainter") ➞ False
# Doesn't show starting word after being collapsed.

valid_word_nest("shape", "sssshapeshapehahapehpeape") ➞ False
# Word placed outside, not inside itself.
Notes
Valid word nests can only be created by repeatedly placing the word inside itself,
so at any point when collapsing the nest, there should only be one instance of
the word to be found.
"""


def valid_word_nest(word, nest):
    """ Functions checks if nest is proper acc.to task description """
    # Decompose word from nest string with each iteration. Stop iteration while
    # nest string consists of only word
    while word != nest:
        # Mandatory condtion: nest string must include word string
        if word not in nest:
            return False
        # Split nest string with word string (acc. to task desc)
        splitted_nest = nest.split(word)
        # Acc.to problem description, nest string must include only ONE word
        if len(splitted_nest) != 2:
            return False
        # Update nest string as glueing splitted_nest
        nest = "".join(splitted_nest)
    return True


if __name__ == "__main__":
    import numpy as np

    np.testing.assert_equal(
        valid_word_nest(
            "redeem",
            "rederedredrredredrerrrederedeememedeemedeemedeeemeemmedeemeemeemem",
        ),
        False,
    )
    np.testing.assert_equal(
        valid_word_nest(
            "survey", "sursursurvsurvssurssursusurveyrveyveyurveyveyurveyeyeyveyvey"
        ),
        True,
    )
    np.testing.assert_equal(
        valid_word_nest(
            "sensation",
            "sensatissenssensastssenensensasenssensensensationsationsationationtionsationatioionantionensationon",
        ),
        False,
    )
    np.testing.assert_equal(valid_word_nest("feed", "feefeeded"), False)
    np.testing.assert_equal(
        valid_word_nest("station", "ststatstasstatistationontationtionionation"), True
    )
    np.testing.assert_equal(valid_word_nest("quarrel", "quaquarrquarrelrerell"), False)
    np.testing.assert_equal(
        valid_word_nest("broadcast", "broadcbroadcastbroadcastast"), False
    )
    np.testing.assert_equal(
        valid_word_nest(
            "current", "currccurrcurcurrcucucurrentrrentrrententrententurrentent"
        ),
        True,
    )
    np.testing.assert_equal(valid_word_nest("diet", "diet"), True)
    np.testing.assert_equal(
        valid_word_nest("park", "pppappappapapapapparkarkarkrkrkrkrkkarkrkrarkark"),
        False,
    )
    np.testing.assert_equal(
        valid_word_nest(
            "undermine",
            "undermiundermundermiunununderundermineminederminedermineneinene",
        ),
        True,
    )
    np.testing.assert_equal(valid_word_nest("nail", "nannnailnailailil"), False)
    np.testing.assert_equal(valid_word_nest("show", "sshssshowhowhowowhow"), True)
    np.testing.assert_equal(
        valid_word_nest("demand", "dedemdeamademademandndndmandnd"), False
    )
    np.testing.assert_equal(
        valid_word_nest("publicity", "publicppublicityublicityity"), True
    )
    np.testing.assert_equal(
        valid_word_nest(
            "relief", "rrerelirerreerrereliefliefelielifliefliefefliefelfeief"
        ),
        False,
    )
    np.testing.assert_equal(valid_word_nest("pipe", "ppppppipeipeipeipeipeipe"), True)
    np.testing.assert_equal(
        valid_word_nest("diagram", "diargdiadidiadiagramgramagramgramam"), False
    )
    np.testing.assert_equal(valid_word_nest("salt", "ssaltalt"), True)
    np.testing.assert_equal(
        valid_word_nest(
            "pioneer",
            "pionpippipioppionpiopipioneeroneerneereerioneerneeroneerioneeroneereer",
        ),
        True,
    )