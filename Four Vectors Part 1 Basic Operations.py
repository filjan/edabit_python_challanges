"""
https://edabit.com/challenge/cPxexgGxmCMi4kas8

Four Vectors Part 1: Basic Operations
This is the first challenge of the "Four Vectors" collection. Four Vectors are
vectors with four components that are used to describe relativistic physics.
For details please refer to this wiki entry.

In this challenge, create a class FourVector with the following properties:

If called with a list of length 4 as a parameter, it uses the list entries as
components of the FourVector (FV) instance.
If called without a parameter, the components should be [0.0, 0.0, 0.0, 0.0].
Getter and Setter methods GetComponents and SetComponents, see test cases
Methods two add and subtract two FV instances.
Support comparing two Four Vectors.
Support printing a FV in the form (0.5, 1.0, -2.0, 10.0) where the components
are rounded to three decimal places.
Consider using magic methods like __add__, __eq__ and __str__ to solve the
last three bullet points.

Examples
v = FourVector([1, 2, 3, 4])
print(v) ➞ (1, 2, 3, 4)

v2 = FourVector([1, 0, 1, 0])
print(v + v2) ➞ (2, 2, 4, 4)
Notes
Please save your FourVector class for later use, we will add new methods in
upcoming challenges in this series!
"""

import functools
from typing import List


def check_given_vector_correctness(fun):
    """
    Additional decorator for checking correctness of given arguments to setters
    methods
    """
    @functools.wraps(fun)
    def wrapper(self, *new_components):
        # This if activates only when __init__ is called
        if fun.__name__ == "__init__" and not new_components:
            return fun(self, [0,0,0,0])
        # Check if argument is list
        if not isinstance(*new_components, list):
            raise Exception("Given arguments is not list")
        # Check if given list has 4 components
        if len(*new_components) != 4:
            raise Exception("Given sequence has inappropriate length")
        # Check if given sequence has only numeric values
        if not all(map(lambda x: isinstance(x, int), *new_components)) \
            and not all(map(lambda x: isinstance(x, float), *new_components)):
            raise Exception("Given types of values in sequence are not consistent")
        return fun(self, *new_components)
    return wrapper


class FourVector:
    """
    Class decribes four value vector
    """
    @check_given_vector_correctness
    def __init__(self, components):
        self._components = components


    def GetComponents(self):
        """
        Getter method required by edabit test
        Using decorators is more sophisticated but not aimed by edabit tests !
        NO PEP name intentionally (acc. to task descr.)
        """
        return self._components

    @check_given_vector_correctness
    def SetComponents(self, new_components):
        """
        Setter method required by edabit test
        Using decorators is more sophisticated but not aimed by edabit tests !
        NO PEP name intentionally (acc. to task descr.)
        """
        self._components = new_components

    @property
    def GetComponents_2(self):
        """
        Additional getter method
        NO PEP name intentionally (acc. to task descr.)
        """
        return self._components

    @GetComponents_2.setter
    @check_given_vector_correctness
    def SetComponents_2(self, new_components):
        """
        Additional setter method
        NO PEP name intentionally (acc. to task descr.)
        """
        self._components = new_components

    def __eq__(self, other):
        """
        Compare two vectors - each adequate component must be equal.
        Return bool value.
        """
        zipped_comps = zip(self.GetComponents_2, other.GetComponents_2)
        return all(map(lambda xy: xy[0] == xy[1], zipped_comps))

    def __add__(self, other):
        """
        Add two vectors
        Return new instance of FourVector with sum as initial components.
        """
        zipped_comps = zip(self.GetComponents_2, other.GetComponents_2)
        sum_of_components = list(map(lambda xy: xy[0] + xy[1], zipped_comps))
        return FourVector(sum_of_components)

    def __sub__(self, other):
        """
        Subtract two vectors
        Return new instance of FourVector with difference between
        initial components.
        """
        zipped_comps = zip(self.GetComponents_2, other.GetComponents_2)
        dif_of_components = list(map(lambda xy: xy[0] - xy[1],zipped_comps))
        return FourVector(dif_of_components)

    def __repr__(self):
        """
        Return string as '(val1, val2, val3, val4)'
        """
        repr_string = tuple(round(val, 3) for val in self.GetComponents_2)
        return f"{repr_string}"

    def __str__(self):
        """
        Call __repr__
        """
        return self.__repr__()


if __name__ == "__main__":
    a = FourVector([1, 2, 3, 4])
    b = FourVector()

    # create some Four Vectors:
    v0a = FourVector()
    v0 = FourVector([0, 0, 0, 0])
    v1 = FourVector([1, 2, 3, 4])
    v2 = FourVector([1, 0, 0, 1])
    v3 = FourVector([1, 0, 1, 0])
    v4 = FourVector([-1, 4, 1, 2])
    v5 = FourVector([-1, 37, 55, -108])
    v6 = FourVector([0.5, 1.0, -2.0, 10.0])
    v7 = FourVector([-0.25, 1.2, -2.7, 33.3])
    v8 = FourVector([-1, 2, -3, 4])

    import numpy as np

    # Edabit tests:
    # # getter and setter operations:
    np.testing.assert_equal(
        v5.GetComponents(), [-1, 37, 55, -108], "v5 components don't match!"
    )
    np.testing.assert_equal(
        v8.GetComponents(), [-1, 2, -3, 4], "v8 components don't match!"
    )
    v8.SetComponents([7, 3, 5, -6])
    np.testing.assert_equal(
        v8.GetComponents(), [7, 3, 5, -6], "modified v8 components don't match!"
    )
    v9 = FourVector()
    np.testing.assert_equal(
        v9.GetComponents(), [0, 0, 0, 0], "new FV components don't match!"
    )
    v9.SetComponents([2.5, -1.5, 3.7, -7.4])
    np.testing.assert_equal(
        v9.GetComponents(),
        [2.5, -1.5, 3.7, -7.4],
        "v9 modified components don't match!",
    )

    # # plus operation:
    np.testing.assert_equal(v0, v0a, "equality of zero vectors don't match!")
    np.testing.assert_equal(
        v0 + v1, FourVector([1, 2, 3, 4]), "addition v0 + v1 doesn't match!"
    )
    np.testing.assert_equal(
        v2 + v3, FourVector([2, 0, 1, 1]), "addition v2 + v3 doesn't match!"
    )
    np.testing.assert_equal(
        v2 + v5, FourVector([0, 37, 55, -107]), "addition v2 + v5 doesn't match!"
    )
    np.testing.assert_equal(
        v2 + v5 + v4,
        FourVector([-1, 41, 56, -105]),
        "addition v2 + v5 + v4 doesn't match!",
    )
    np.testing.assert_equal(
        v0 + v1 + v2 + v3 + v4 + v5,
        FourVector([1, 43, 60, -101]),
        "addition v0 + v1 + v2 + v3 + v4 + v5 doesn't match!",
    )
    np.testing.assert_equal(
        v1 + v6, FourVector([1.5, 3.0, 1.0, 14.0]), "addition v1 + v6 doesn't match!"
    )
    np.testing.assert_equal(
        v6 + v7, FourVector([0.25, 2.2, -4.7, 43.3]), "addition v6 + v7 doesn't match!"
    )
    np.testing.assert_equal(
        v0 + v4 + v6 + v7,
        FourVector([-0.75, 6.2, -3.7, 45.3]),
        "addition v0 + v4 + v6 + v7 doesn't match!",
    )

    # minus operation:
    np.testing.assert_equal(
        v2 - v1, FourVector([0, -2, -3, -3]), "subtraction v2 - v1 doesn't match!"
    )
    np.testing.assert_equal(
        v4 - v2, FourVector([-2, 4, 1, 1]), "subtraction v4 - v2 doesn't match!"
    )
    np.testing.assert_equal(
        str(v4 - v7),
        str(FourVector([-0.75, 2.8, 3.7, -31.3])),
        "subtraction v4 - v7 doesn't match!",
    )
    np.testing.assert_equal(
        v5 - v0, FourVector([-1, 37, 55, -108]), "subtraction v5 - v0 doesn't match!"
    )
    np.testing.assert_equal(
        v5 - v1 - v6,
        FourVector([-2.5, 34.0, 54.0, -122.0]),
        "subtraction v5 - v1 - v6 doesn't match!",
    )

    # mixed operations:
    np.testing.assert_equal(
        v5 - v2 + v4, FourVector([-3, 41, 56, -107]), "mixed operation 1 doesn't match!"
    )
    np.testing.assert_equal(v6 + v3 - v6, v3, "mixed operation 2 doesn't match!")
    np.testing.assert_equal(
        v5 - v7 + v1 - v0 + v2,
        FourVector([1.25, 37.8, 60.7, -136.3]),
        "mixed operation 3 doesn't match!",
    )

    # string representation:
    np.testing.assert_equal(
        str(v1), "(1, 2, 3, 4)", "string representation of v1 doesn't match!"
    )
    np.testing.assert_equal(
        str(v6), "(0.5, 1.0, -2.0, 10.0)", "string representation of v6 doesn't match!"
    )
    np.testing.assert_equal(
        str(v0 + v4 + v6 + v7),
        "(-0.75, 6.2, -3.7, 45.3)",
        "string representation of v0 + v4 + v6 + v7 doesn't match!",
    )
