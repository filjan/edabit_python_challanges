"""
https://edabit.com/challenge/D93PLXfXyfzByMnTm
Four Points Make a Square
The function is given four points with (x, y) coordinates in no particular order.
Determine if these points make a square and return True / False.

A square has four equal sides with positive length and four 90-degree angles.

Examples
valid_square((0, 0), (1, 1), (1, 0), (0, 1)) ➞ True

valid_square((0, 0), (1, 1), (1, 0), (0, 12)) ➞ False

valid_square((1, 0), (-1, 0), (0, 1), (0, -1)) ➞ True

valid_square((0, 0), (0, 0), (0, 0), (0, 0)) ➞ False
Notes
A square also has equal diagonals.
"""

from collections import namedtuple


def valid_square(*points_coordinates):
    """ Function checks if given points are corners of the square """
    # Check correctness of the given value (repetitive same points and
    # number of four points)
    if (
        len(set(points_coordinates)) != len(points_coordinates)
        or len(points_coordinates) != 4
    ):
        return False
    # Declare class of Point, as container for coordinates
    Point = namedtuple("Point", ["x", "y"])
    # Convert tuple with coordinates into list of Points
    points = list(map(lambda coors: Point(x=coors[0], y=coors[1]), points_coordinates))
    # Sort by x coordinate and then by y coordinate
    points.sort(key=lambda p: (p.x, p.y))
    # Declare class of diagonals with tuple of two corners
    Diagonal = namedtuple("Diagonal", ["corners"])
    # Create list with two diagonal instances
    diagonals = [
        Diagonal(corners=(points[0], points[-1])),
        Diagonal(corners=(points[1], points[2])),
    ]
    # Define function which calculates length between two points
    calculate_diag_length = lambda c_1, c_2: (
        (c_2.x - c_1.x) ** 2 + (c_2.y - c_1.y) ** 2
    ) ** (1 / 2)
    # Calculate lengths of two diagonals
    lengths = tuple(map(lambda d: calculate_diag_length(*d.corners), diagonals))
    # Compare lengths. If they have equale lengths, it has to be quare
    return bool(lengths[0] == lengths[1])

if __name__ == "__main__":

    from time import perf_counter
    import numpy as np

    tic = perf_counter()

    np.testing.assert_equal(valid_square((0, 0), (1, 1), (1, 0), (0, 1)), True)
    np.testing.assert_equal(valid_square((0, 0), (1, 1), (1, 0), (0, 12)), False)
    np.testing.assert_equal(valid_square((1, 0), (-1, 0), (0, 1), (0, -1)), True)
    np.testing.assert_equal(valid_square((0, 0), (0, 0), (0, 0), (0, 0)), False)
    np.testing.assert_equal(valid_square((0, 4), (4, 6), (3, 3), (1, 7)), True)
    np.testing.assert_equal(valid_square((11, 23), (16, 32), (18, 18), (23, 25)), False)
    np.testing.assert_equal(valid_square((2, 2), (8, 15), (-11, 8), (-5, 21)), True)
    np.testing.assert_equal(valid_square((-16, 20), (-18, 3), (-1, -1), (3, 16)), False)
    np.testing.assert_equal(
        valid_square((-28, 16), (-27, 11), (-22, 17), (-21, 11)), False
    )
    np.testing.assert_equal(valid_square((41, -37), (49, 10), (2, 18), (-6, -29)), True)
    np.testing.assert_equal(
        valid_square((-74, 89), (-28, 71), (-46, 25), (-92, 43)), True
    )
    np.testing.assert_equal(
        valid_square((23, 36), (1, -36), (45, -14), (-21, 8)), False
    )
    np.testing.assert_equal(
        valid_square((-29, 88), (-43, 19), (-112, 33), (-98, 102)), True
    )
    np.testing.assert_equal(
        valid_square((15, 125), (-86, 133), (-31, 179), (-41, 78)), False
    )
    np.testing.assert_equal(
        valid_square((-149, 115), (-28, 158), (-110, 197), (-67, 76)), True
    )
    np.testing.assert_equal(
        valid_square((148, 349), (169, 96), (32, 212), (285, 233)), True
    )
    np.testing.assert_equal(
        valid_square((-84, 19), (56, -48), (-2, -187), (-141, -130)), False
    )
    np.testing.assert_equal(
        valid_square((77, -133), (42, 164), (207, 34), (-91, -3)), False
    )
    np.testing.assert_equal(
        valid_square((-922, 84), (-1088, 1061), (55, 250), (-111, 1227)), True
    )
    np.testing.assert_equal(
        valid_square((53, 2547), (22, 787), (919, 1650), (-840, 1681)), False
    )
    np.testing.assert_equal(
        valid_square((-1040, 1021), (-1069, 729), (-748, 992), (-777, 700)), True
    )

    print("t_sec = {:.6f}".format(perf_counter() - tic))