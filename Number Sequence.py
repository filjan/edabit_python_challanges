"""
https://edabit.com/challenge/dBBhtQqKZb2eDERHg

Number Sequence
Write a recursive function that accepts an integer n and return a sequence of
n integers as a string, descending from n to 1 and then ascending back from 1
to n as in the examples below:

Examples
number_sequence(1) ➞ "1"

number_sequence(2) ➞ "1 1"

number_sequence(3) ➞ "2 1 2"

number_sequence(4) ➞ "2 1 1 2"

number_sequence(9) ➞ "5 4 3 2 1 2 3 4 5"

number_sequence(10) ➞ "5 4 3 2 1 1 2 3 4 5"

Notes
Only use recursion.
No auxiliary data structures like list and tuple are allowed.

"""

def number_sequence(val, __word=""):
    """
    Function returns mirro-looked string with val numbers from 1 to ceil(val/2)
    with double or not 1 in the center
    """
    # If given value is negative return '-1'
    if val <= 0:
        return "-1"
    # If length of word is equal to double first value, than return
    if len(__word) == 2 * val - 1:
        return __word
    # Initialize center of string according to val evennes or extend
    # __word by increasing figures
    if not val % 2 and __word == "":
        __word = "1 1"
    elif val % 2 and __word == "":
        __word = "1"
    else:
        last_figure = int(__word.split(" ")[0])
        __word = f"{last_figure+1} {__word} {last_figure+1}"
    # Return itself recursively
    return number_sequence(val, __word)


if __name__ == "__main__":

    #
    import numpy as np
    np.testing.assert_equal(number_sequence(12), "6 5 4 3 2 1 1 2 3 4 5 6")

    ### Edabit tests
    np.testing.assert_equal(number_sequence(1), "1")
    np.testing.assert_equal(number_sequence(2), "1 1")
    np.testing.assert_equal(number_sequence(3), "2 1 2")
    np.testing.assert_equal(number_sequence(4), "2 1 1 2")
    np.testing.assert_equal(number_sequence(5), "3 2 1 2 3")
    np.testing.assert_equal(number_sequence(6), "3 2 1 1 2 3")
    np.testing.assert_equal(number_sequence(7), "4 3 2 1 2 3 4")
    np.testing.assert_equal(number_sequence(8), "4 3 2 1 1 2 3 4")
    np.testing.assert_equal(number_sequence(9), "5 4 3 2 1 2 3 4 5")
    np.testing.assert_equal(number_sequence(10), "5 4 3 2 1 1 2 3 4 5")
    np.testing.assert_equal(number_sequence(0), "-1")
    np.testing.assert_equal(number_sequence(-15), "-1")