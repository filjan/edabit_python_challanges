"""
https://edabit.com/challenge/p8iNsRCtj3AdJsYjS
Excel Sheet Column Number
Given a column title as it appears in an Excel sheet return its corresponding 
column number.

The number is computed in the following way:

A -> 1
B -> 2
C -> 3
...
Z -> 26
AA -> 27
AB -> 28
...
Examples
title_to_number("A") ➞ 1

title_to_number("R") ➞ 18

title_to_number("AB") ➞ 28
Notes
1 <= len(s) <= 7
s consists only of uppercase English letters.
"""


def title_to_number(title):
    """ Function returns number of given column """
    # Convert each letter in title into specific exel value (like separated values)
    single_values = [ord(letter) - 64 for letter in title]
    # Define indices of given letters
    single_values_indices = list(range(len(single_values)))
    # Convert indices into excel-specific system type with base 26
    weights = list(map(lambda x: 26 ** x, single_values_indices[::-1]))
    # Multiply weights of place in order by single values
    partial_values = [zipped[0] * zipped[1] for zipped in zip(single_values, weights)]
    # Sum partial values into one
    return sum(partial_values)


if __name__ == "__main__":

    import numpy as np
    from time import perf_counter

    tic = perf_counter()

    ### Edabit tests
    np.testing.assert_equal(title_to_number("A"), 1)
    np.testing.assert_equal(title_to_number("R"), 18)
    np.testing.assert_equal(title_to_number("AB"), 28)
    np.testing.assert_equal(title_to_number("ZY"), 701)
    np.testing.assert_equal(title_to_number("KFC"), 7595)
    np.testing.assert_equal(title_to_number("WEB"), 15680)
    np.testing.assert_equal(title_to_number("FANG"), 106503)
    np.testing.assert_equal(title_to_number("CODE"), 62977)
    np.testing.assert_equal(title_to_number("AGILE"), 586409)
    np.testing.assert_equal(title_to_number("TRUMP"), 9470438)
    np.testing.assert_equal(title_to_number("EDABIT"), 61253966)
    np.testing.assert_equal(title_to_number("PYTHON"), 201883748)
    np.testing.assert_equal(title_to_number("ANISTON"), 479715692)
    np.testing.assert_equal(title_to_number("FRIENDS"), 2071569675)

    print("t = {:.6f}".format(perf_counter() - tic))