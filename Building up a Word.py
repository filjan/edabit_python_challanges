"""
https://edabit.com/challenge/zQespQxTsiGoeMNP3
Building up a Word
You are given an input list of strings, ordered by ascending length.

Write a function that returns True if, for each pair of consecutive strings,
the second string can be formed from the first by adding a single letter
either at the beginning or end.

Examples
can_build(["a", "at", "ate", "late", "plate", "plates"]) ➞ True

can_build(["a", "at", "ate", "late", "plate", "plater", "platter"]) ➞ False
# "platter" is formed by adding "t" in the middle of "plater"

can_build(["it", "bit", "bite", "biters"]) ➞ False
# "biters" is formed by adding two letters - we can only add one

can_build(["mean", "meany"]) ➞ True
Notes
Return False if a word is NOT formed by adding only one letter.
Return False if the letter is added to the middle of the previous word.
Letters in tests will all be lower case.
"""


def can_build(words):
    """
    Functions returns True if consecutive word is able to be created
    by previous one with added one letter
    """
    # If given list has only one component, return True
    if len(words) == 1:
        return True
    # Check if considered word is same as next one without last or first letters
    # if yes recur function with shorter input list, if not -> return False
    if words[0] == words[1][:-1] or words[0] == words[1][1:]:
        return can_build(words[1:])
    else:
        return False


if __name__ == "__main__":
    print(can_build(["a", "at", "ate", "late", "plate", "plates"]))
    print(can_build(["a", "at", "ate", "late", "plate", "plater", "platter"]))
    print(can_build(["it", "bit", "bite", "biters"]))
    print(can_build(["mean", "meany"]))