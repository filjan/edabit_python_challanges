"""
https://edabit.com/challenge/SbXSM5HwdkgENLEwY

Class That Takes Selfies
Implement a class Selfie that can store the current state of the object in the
form of binary string. It can take multiple pictures and then recover to a
state it was before. During testing an object will be provided with new
attributes and their values. It will store its state. Then the values will
be changed. Then it will be given new attributes. It will store its state
again. It will be repeated few times.

Later the states of the object will be recovered given an index. The return
value should be a new Selfie with the requested historic state and the state
history of the new object should be updated with a copy of current object's
state history.

The object also knows how many states it has stored. If the index is not
within the range of stored states, the object stays as is. If the argument
is invalid, n < 0 or n >= self.n_states(), the current object (or a copy
thereof) should be returned.

Examples
p = Selfie()
p.x = 2
p.save_state()
p.x = 5
p = p.recover_state(0)
p.x ➞ 2
Notes
Use of global variables outside the class is not allowed.
When an object is restored to a previous state it keeps all saved states.

"""

from copy import deepcopy


class Selfie:
    """ Selfie class definition """

    def __init__(self):
        """
        Initialize  lst_state as list with first dict inside.
        Each dict in list lst_state include defined attributes.
        """
        self.lst_state = [{}]

    def save_state(self):
        """
        Append new empty dict. The previous one means previous state
        """
        self.lst_state.append({})

    def recover_state(self, n):
        """
        Function recovers attributes from chosen previous state.
        (to tell the truth it returns new Selfie instance with appropriate
        variables and all saved states)
        """
        # Check correctness of chosen n - i.e. state number to retrieve
        if n < 0 or n >= self.n_states():
            return self
        #
        recovered_instance = Selfie()
        # Copy lst_state for new Selfie with retreived state
        states_to_copy = deepcopy(self.lst_state)
        # Read attibutes in n-state
        vars_to_recover = self.lst_state[n]
        # Iteratively assign attributes to the new instance
        for key, val in vars_to_recover.items():
            setattr(recovered_instance, key, val)
        # Overwrite lst_state from retreving instance
        setattr(recovered_instance, "lst_state", states_to_copy)
        # Append empty dict to the end of lst_state(if it's not there already)
        if recovered_instance.lst_state[-1] != {}:
            recovered_instance.lst_state.append({})
        #
        return recovered_instance

    def n_states(self):
        """Return length of lst_state minus 1 (last one include attributes,
        before save)
        """
        return len(self.lst_state) - 1

    def __setattr__(self, name, value):
        """
        Overload setattr method - additionally append new created attribute
        to the last dictionary in lst_state
        """
        if name != "lst_state":
            self.lst_state[-1][name] = value
        super().__setattr__(name, value)


if __name__ == "__main__":

    import numpy as np

    from random import randint
    import time

    lst_test = [
        (
            {"x": 13, "d1": {"a": 1, "b": 4}, "s1": "they like"},
            {"y": 11, "d2": {"m": 15, "n": 14}, "s2": "where to"},
            {"z": 10, "d3": {"intercept": 6.4, "slope": 0.7}, "tpl": (5, 7)},
        ),
        (
            {"f": 1.6, "l1": [1, 3, 5], "l2": [24, 25]},
            {"my_tpl": (1, 1), "s1": "we know", "s2": "how to do it"},
        ),
        (
            {"s1": "laptop", "s2": "has", "s3": "screen and keyboard", "s4": "!"},
            {"a1": 11, "a2": 13, "a3": 17, "a4": 19, "a5": 23, "a6": 29},
            {"l1": [1, 3, 5], "l2": [24, 25], "l3": [33], "l4": [42, 44, 46, 48]},
            {"st1": {99, 88, 77}, "tpl37": (3, 7), "dct72": {"q": 12, "w": 5}},
        ),
    ]

    tic = time.perf_counter()

    p = Selfie()
    p.x = 2
    p.save_state()
    p.x = 5
    p = p.recover_state(0)
    np.testing.assert_equal(p.x, 2)

    for t in lst_test:
        p = Selfie()
        for d in t:
            s = p.lst_state.copy()

            """assign given attributes"""
            [
                setattr(p, key, val) for key, val in d.items()
            ]  # WRONG TEST ! -> p.__dict__ = d.copy()
            p.__dict__.update({"lst_state": s})

            p.save_state()

            """change the values of attributes"""
            for k in p.__dict__.keys():
                if k != "lst_state":
                    p.__dict__[k] = randint(0, 100)

            """check that the object is different now"""
            state_dict = p.__dict__.copy()
            del state_dict["lst_state"]
            np.testing.assert_equal(state_dict != d, True)

        """check the number of saved states"""
        np.testing.assert_equal(p.n_states(), len(t))

        # """check the type of stored states""" # WRONG TEST ! -> its unconsistent with given data
        # np.testing.assert_equal(all(type(val) == bytes for val in p.lst_state), True)

        """check the saved states"""
        for i, d in enumerate(t):
            p = eval("p.recover_state(i)", {"p": p, "i": i})
            state_dict = p.__dict__.copy()
            del state_dict["lst_state"]
            np.testing.assert_equal(state_dict, d)

        """try to recover to incorrect index of saved states"""
        p = eval("p.recover_state(idx)", {"p": p, "idx": len(t) + randint(1, 10)})
        state_dict = p.__dict__.copy()
        del state_dict["lst_state"]
        np.testing.assert_equal(state_dict, t[-1])

    print("Runtime(sec) = {:.6f}".format(time.perf_counter() - tic))
