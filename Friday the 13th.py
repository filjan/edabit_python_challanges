"""
https://edabit.com/challenge/Xkc2iAjwCap2z9N5D
Friday the 13th
Given the month and year as numbers, return whether that month contains a Friday 13th.

Examples
has_friday_13(3, 2020) ➞ True

has_friday_13(10, 2017) ➞ True

has_friday_13(1, 1985) ➞ False
Notes
January will be given as 1, February as 2, etc ...
Check Resources for some helpful tutorials on Python's datetime module.
"""

import datetime as dt


def check_arguments_correctness(func):
    """Wrapper for checking arguments correctness"""

    def wrapper(*args):
        assert args[0] <= 12, "Month argument must be less than 12"
        assert args[0] > 0, "Month argument must be positive"
        assert args[1] > 0, "Year argument mus be positive"
        return func(*args)

    return wrapper


@check_arguments_correctness
def has_friday_13(month, year):
    """
    Function checks existence of friday as 13th consecutive day within given year and month
    """
    # Determine number of days during considered month
    number_of_days_in_the_month = (dt.date(year, month, 1) - dt.timedelta(days=1)).day
    # Define list of numbers of each day within month
    consecutive_dates = list(
        map(lambda x: dt.date(year, month, x + 1), range(number_of_days_in_the_month))
    )
    # Define list of small tuples - with number day within week, and within month
    consecutive_days_and_weekdays = list(
        map(lambda date: (date.day, date.weekday()), consecutive_dates)
    )
    # Check existence of 13th number of day in friday
    return (13, 4) in consecutive_days_and_weekdays


if __name__ == "__main__":
    print(has_friday_13(3, 2020))
    print(has_friday_13(10, 2017))
    print(has_friday_13(1, 1985))

    print(has_friday_13(1, 10))
