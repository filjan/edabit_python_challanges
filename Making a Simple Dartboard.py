"""
https://edabit.com/challenge/Kv8DMmwfuKTLyZD5E
Making a Simple Dartboard
Create a function which creates a square dartboard of side length n. The value
of a number should increase, the closer it is to the centre of the board.

Examples
make_dartboard(3) ➞ [
  111,
  121,
  111
]

make_dartboard(8) ➞ [
  11111111,
  12222221,
  12333321,
  12344321,
  12344321,
  12333321,
  12222221,
  11111111
]

make_dartboard(5) ➞ [
  11111,
  12221,
  12321,
  12221,
  11111
]
Notes
If the size given is an even number, the centre should be made up of the 4
highest values.
"""

import numpy as np


def make_dartboard(n):
    """
    Function make dateboard like list of integers with given side dimension n
    """
    if n <= 0 or not isinstance(n, int):
        raise Exception("n number must be integer bigger than 0")
    # Floor half n
    n_floor = int(np.floor(n / 2))
    # Ceil half n
    n_ceil = int(np.ceil(n / 2))
    # Declare dartboard (as empty list now)
    dartboard = []
    # Declare list with length as half (or almost half) of dartboard width
    half_row = [0] * n_floor
    # Create in a loop one quarter of double symmetric part of dartboard
    for num_row in range(n_floor):
        # Copy half_row, as iteratively it's the same with changed values after
        # some index
        half_row = half_row.copy()
        # Overwrite values after num_col with number of dartboard row + 1
        for num_col in range(num_row, len(half_row)):
            half_row[num_col] = num_row + 1
        #
        dartboard.append(half_row)
        #
    # Extend quarter of dartboard relatively to vertical axis symmetry as
    # extending each row by mirror view of itself
    dartboard = [dartboard[index] + row[::-1] for index, row in enumerate(dartboard)]
    # Extend half of dartboard relatively to horizontal axis
    dartboard.extend(dartboard[::-1])
    # Determine evenness of dimension n. Even width to add one middle col and row
    if n % 2 != 0:
        # Determine half of diagonal (with middle component)
        half_diagonal = list(range(1, n_ceil + 1))
        # Iterate over rows of current dartboard and insert in the middle
        # appropriate value from diagonal_withouth_middle.
        # BE CAREFUL! Half of the dartboard has mutable views of previous rows!
        for i, (insert_num, row) in enumerate(zip(half_diagonal[:-1], dartboard)):
            # Insert insert_num into middle of each row (as on the other
            # half there are views, insert may concern only one half)
            row.insert(int(len(row) // 2), insert_num)
        # Determine full diagonal as mirror of half_diagonal with one middle value
        diagonal = half_diagonal + half_diagonal[::-1][1:]
        # Insert horizontal row
        dartboard.insert( int(len(dartboard)) // 2, diagonal )
    # Convert internal list(rows of dartboard) into string
    dartboard = list(map(lambda x: "".join(map(str, x)), dartboard))
    # Convert string (as rows) into integers
    dartboard = list(map(lambda x: int(x), dartboard))
    return dartboard


if __name__ == "__main__":
    a = make_dartboard(5)
    b = a.copy()

    np.testing.assert_equal(make_dartboard(3), [111, 121, 111])

    np.testing.assert_equal(
        make_dartboard(8),
        [
            11111111,
            12222221,
            12333321,
            12344321,
            12344321,
            12333321,
            12222221,
            11111111,
        ],
    )

    np.testing.assert_equal(make_dartboard(5), [11111, 12221, 12321, 12221, 11111])

    np.testing.assert_equal(make_dartboard(2), [11, 11,])

    np.testing.assert_equal(make_dartboard(1), [1])

    np.testing.assert_equal(make_dartboard(4), [1111, 1221, 1221, 1111])

    np.testing.assert_equal(
        make_dartboard(6), [111111, 122221, 123321, 123321, 122221, 111111]
    )

    np.testing.assert_equal(
        make_dartboard(7),
        [1111111, 1222221, 1233321, 1234321, 1233321, 1222221, 1111111],
    )

    np.testing.assert_equal(
        make_dartboard(9),
        [
            111111111,
            122222221,
            123333321,
            123444321,
            123454321,
            123444321,
            123333321,
            122222221,
            111111111,
        ],
    )