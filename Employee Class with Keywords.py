"""
https://edabit.com/challenge/S7rdJsn6vkfC9BzcR
Employee Class with Keywords
Create a class Employee that will take a full name as argument, as well as
a set of none, one or more keywords. Each instance should have a name and a
lastname attributes plus one more attribute for each of the keywords, if any.

Examples
john = Employee("John Doe")
mary = Employee("Mary Major", salary=120000)
richard = Employee("Richard Roe", salary=110000, height=178)
giancarlo = Employee("Giancarlo Rossi", salary=115000, height=182,
                      nationality="Italian")

john.name ➞ "John"
mary.lastname ➞ "Major"
richard.height ➞ 178
giancarlo.nationality ➞ "Italian"
Notes
First and last names will be separated by a whitespace. The test will not
include any middle names or initials.
The value of the keywords can be an int, a str or a list.

"""

class Employee:
    """ Employee class """

    def __init__(self, full_name, **kwargs):
        """
        Extract input arguments. Extract first name, lastname and call
        process_kwargs_attributes()
        """
        self.name = full_name.split(" ")[0]
        self.lastname = full_name.split(" ")[-1]
        self.process_given_attributes(kwargs)

    def process_given_attributes(self, given_kwargs):
        """ Process given argument keyword into instance attributes """
        _ = {setattr(self, key, val) for key, val in given_kwargs.items()}


if __name__ == "__main__":
    import numpy as np

    john = Employee("John Doe")
    mary = Employee("Mary Major", salary=120000)
    richard = Employee("Richard Roe", salary=110000, height=178)
    giancarlo = Employee(
        "Giancarlo Rossi", salary=115000, height=182, nationality="Italian"
    )
    peng = Employee(
        "Peng Zhu",
        salary=500000,
        height=185,
        nationality="Chinese",
        subordinates=[i.lastname for i in (john, mary, richard, giancarlo)],
    )

    np.testing.assert_equal(john.lastname, "Doe", 'John\'s lastname should be "Doe"')
    np.testing.assert_equal(mary.salary, 120000, "Mary's salary should be 120000")
    np.testing.assert_equal(richard.height, 178, "Richard's height should be 178")
    np.testing.assert_equal(
        giancarlo.nationality, "Italian", 'Giancarlo\'s nationality should be "Italian"'
    )
    np.testing.assert_equal(
        peng.subordinates,
        ["Doe", "Major", "Roe", "Rossi"],
        "Peng's subordinates should be a list containing 4 strings",
    )