"""
https://edabit.com/challenge/xuuQfgva9CKDH6iwH

Plant the Grass
You will be given a matrix representing a field g and two numbers x, y
coordinate.

There are three types of possible characters in the matrix:

x representing a rock.
o representing a dirt space.
+ representing a grassed space.
You have to simulate grass growing from the position (x, y). Grass can grow in
all four directions (up, left, right, down). Grass can only grow on dirt
spaces and can't go past rocks.

Return the simulated matrix.

Examples
simulate_grass([
  "xxxxxxx",
  "xooooox",
  "xxxxoox"
  "xoooxxx"
  "xxxxxxx"
], 1, 1) ➞ [
  "xxxxxxx",
  "x+++++x",
  "xxxx++x"
  "xoooxxx"
  "xxxxxxx"
]
Notes
There will always be rocks on the perimeter

My notes:

    Coordinate:     x <---->

                      ^
                    y |
                      v

    Orientation
                      p[y-1][x]

                        ^
                        | N
                W                E
    p[y][x-1]   <-    p[y][x]    -> p[y][x+1]

                        | S
                        v

                      p[y+1][z]

"""


def replace(input_string, index):
    """
    Function return string with replaced character positioned in given index
    by '+' character
    """
    return input_string[:index] + "+" + input_string[index + 1 :]


def simulate_grass(g, x, y):
    """
    Each tuft of grass broadcasts in each direction so it's possible
    to treat consecutive tufts recurrently with altered input
    """
    # Make temporary copy of the board in the given start state
    g_check = g.copy()
    # Check if given position is dirt space - if yes cover it with grass
    if g[y][x] == "o":
        g[y] = replace(g[y], x)
    # N - Check if tuft on the north is grassable:
    elif g[y - 1][x] == "o":
        simulate_grass(g, x, y - 1)
    # W - Check if tuft on the west is grassable:
    elif g[y][x - 1] == "o":
        simulate_grass(g, x - 1, y)
    # S - Check if tuft on the south is grassable:
    elif g[y + 1][x] == "o":
        simulate_grass(g, x, y + 1)
    # E - Check if tuft on the east is grassable:
    elif g[y][x + 1] == "o":
        simulate_grass(g, x + 1, y)
    # Check if board has been altered, if yes check the possibility of
    # occurence grassable tufts again
    if g_check != g:
        simulate_grass(g, x, y)
    # If nothing has changed, return current board
    return g


if __name__ == "__main__":

    simulate_grass([
                "xxxxxxx",
                "xoxooox",
                "xoxooox",
                "xxxxxxx",
            ], 2, 1,)

    import numpy as np

    # My test
    np.testing.assert_equal(
        simulate_grass(
            [
                "xxxxxxx",
                "xoxooox",
                "xoxooox",
                "xxxxxxx",
            ], 3, 1,
        ),
            [
                "xxxxxxx",
                "xox+++x",
                "xox+++x",
                "xxxxxxx",
            ],
            )

    np.testing.assert_equal(
        simulate_grass(
            [
                "xxxxxxx",
                "xoxooox",
                "xoxooox",
                "xxxxxxx",
            ], 2, 1,
        ),
            [
                "xxxxxxx",
                "x+x+++x",
                "x+x+++x",
                "xxxxxxx",
            ],
            )

    np.testing.assert_equal(
        simulate_grass(
            [
                "xxxxxxx",
                "xxxooox",
                "xxxooox",
                "xxxxxxx",
            ], 1, 1,
        ),
            [
                "xxxxxxx",
                "xxxooox",
                "xxxooox",
                "xxxxxxx",
            ],
            )

    # edabit.com tests

    np.testing.assert_equal(
        simulate_grass(["xxxxxxx", "xooooox", "xxxxoox", "xoooxxx", "xxxxxxx"], 1, 1),
        ["xxxxxxx", "x+++++x", "xxxx++x", "xoooxxx", "xxxxxxx"],
    )

    np.testing.assert_equal(
        simulate_grass(
            [
                "xxxxxxx",
                "xoxooox",
                "xxoooox",
                "xooxxxx",
                "xoxooox",
                "xoxooox",
                "xxxxxxx",
            ],
            2,
            3,
        ),
        ["xxxxxxx", "xox+++x", "xx++++x", "x++xxxx", "x+xooox", "x+xooox", "xxxxxxx"],
    )

    np.testing.assert_equal(
        simulate_grass(
            ["xxxxxx", "xoxoox", "xxooox", "xoooox", "xoooox", "xxxxxx"], 1, 1
        ),
        ["xxxxxx", "x+xoox", "xxooox", "xoooox", "xoooox", "xxxxxx"],
    )

    np.testing.assert_equal(
        simulate_grass(["xxxxx", "xooox", "xooox", "xooox", "xxxxx"], 1, 1),
        ["xxxxx", "x+++x", "x+++x", "x+++x", "xxxxx"],
    )

    np.testing.assert_equal(
        simulate_grass(
            [
                "xxxxxx",
                "xxxxox",
                "xxooox",
                "xoooxx",
                "xooxxx",
                "xooxxx",
                "xxooox",
                "xxxoxx",
                "xxxxxx",
            ],
            4,
            1,
        ),
        [
            "xxxxxx",
            "xxxx+x",
            "xx+++x",
            "x+++xx",
            "x++xxx",
            "x++xxx",
            "xx+++x",
            "xxx+xx",
            "xxxxxx",
        ],
    )

    np.testing.assert_equal(
        simulate_grass(
            [
                "xxxxxxxxxxx",
                "xoxooooooox",
                "xoxoxxxxxox",
                "xoxoxoooxox",
                "xoxoxoxoxox",
                "xoxoxoxoxox",
                "xoxoxxxoxox",
                "xoxoooooxox",
                "xoxxxxxxxox",
                "xooooooooox",
                "xxxxxxxxxxx",
            ],
            1,
            1,
        ),
        [
            "xxxxxxxxxxx",
            "x+x+++++++x",
            "x+x+xxxxx+x",
            "x+x+x+++x+x",
            "x+x+x+x+x+x",
            "x+x+x+x+x+x",
            "x+x+xxx+x+x",
            "x+x+++++x+x",
            "x+xxxxxxx+x",
            "x+++++++++x",
            "xxxxxxxxxxx",
        ],
    )