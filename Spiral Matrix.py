"""
https://edabit.com/challenge/btN6uQc3BTmLgSw5G

Spiral Matrix
In this challenge, transform a string into a spiral contained inside a regular
square matrix. To build the matrix, you are given the dimension of its side:

If the side of the matrix is odd, the spiral starting point will be the exact
center of the matrix.
If the side of the matrix is even, the spiral starting point will be placed in
the lower columns half of the lower rows half.
# "x" represents the matrix center

side = 3 (odd)
matrix = [
  [" ", " ", " "],
  [" ", "x", " "],
  [" ", " ", " "]
]

side = 4 (even)
matrix = [
  [" ", " ", " ", " "],
  [" ", "x", " ", " "],
  [" ", " ", " ", " "],
  [" ", " ", " ", " "],
]
The length of the string has to match exactly the number of cells inside the
matrix:

If the string length is greater than the number of cells, you have to cut off
the unnecessary characters.
If the string length is lower than the number of cells, you have to add a
series of "+" to the end of the string until its length match the number
of cells.
side = 3 (9 cells)
string = "EDABITTEROUS"
# You'll need only "EDABITTER", while "OUS" is discarded.
string = "EDABITTER"

side = 4 (16 cells)
string = "EDABITTEROUS"
# You'll need all the string plus 4 "+" to match the cells.
string = "EDABITTEROUS++++"
Starting from the center that you found, you have to fill a regular square
matrix side * side placing the characters of the given string str, following
a clockwise spiral pattern (first move to the right).

side = 3 (odd)
string = "EDABITTEROUS"
matrix = [
  ["T", "E", "R"],
  ["T", "E", "D"],
  ["I", "B", "A"]
]

side = 4 (even)
string = "EDABITTEROUS"
matrix = [
  ["T", "E", "R", "O"],
  ["T", "E", "D", "U"],
  ["I", "B", "A", "S"],
  ["+", "+", "+", "+"],
]
Examples
spiral_matrix(2, "DOG") ➞ [
  ["D", "O"],
  ["+", "G"]
]

spiral_matrix(3, "COPYRIGHTS") ➞ [
  ["G", "H", "T"],
  ["I", "C", "O"],
  ["R", "Y", "P"]
]

spiral_matrix(4, "SUPERLUMBERJACK") ➞ [
  ["U", "M", "B", "E"],
  ["L", "S", "U", "R"],
  ["R", "E", "P", "J"],
  ["+", "K", "C", "A"]
]
Notes
Remember, the first move from the center is to the right, and then you proceed
clockwise and concentrically.
As given side, you can expect any valid value greater than 1.

"""

import numpy as np


def tab_coor(table, row, col):
    """
    Function returns bool according to given position in the table.
    """
    if row < 0 or col < 0:
        return_value = False
    else:
        try:
            _ = table[row][col]
            if not table[row][col]:
                return_value = False
            else:
                return_value = True
        except IndexError:
            return_value = False

    return return_value


def spiral_matrix(side, word):
    """
    Function returns table (list of nested lists) filled spirally with words
    from 'word' and extra '+' signs.
    Table is 'side' width.
    'word' is sliced in case of being too long.
    """
    # Define maximum length of word
    max_word_length = side * side
    # Cut input word to maximum length (if its necessary)
    word = word[:max_word_length]
    # Fill missing until required length (if its necessary)
    word = word.ljust(max_word_length, "+")
    # Prepare empty table of proper side
    table = [[None for i in range(side)] for j in range(side)]
    # Define center of table (tuple of coordinates)
    row, col = (int(np.ceil(side / 2) - 1), int(np.ceil(side / 2)) - 1)
    #
    # Iterate over table from center outward by helix
    while word:
        # Place first character in position row-col in the table
        table[row][col] = word[0]
        # 1 Starting point - go right
        if (
            not tab_coor(table, row, col - 1)
            and not tab_coor(table, row - 1, col)
            and not tab_coor(table, row, col + 1)
            and not tab_coor(table, row + 1, col)
        ):
            col += 1
        # 2
        elif (
            tab_coor(table, row, col - 1)
            and not tab_coor(table, row - 1, col)
            and not tab_coor(table, row, col + 1)
            and not tab_coor(table, row + 1, col)
        ):
            row += 1
        # 3
        elif (
            not tab_coor(table, row, col - 1)
            and tab_coor(table, row - 1, col)
            and not tab_coor(table, row, col + 1)
            and not tab_coor(table, row + 1, col)
        ):
            col -= 1
        # 4
        elif (
            not tab_coor(table, row, col - 1)
            and tab_coor(table, row - 1, col)
            and tab_coor(table, row, col + 1)
            and not tab_coor(table, row + 1, col)
        ):
            col -= 1
        # 5
        elif (
            not tab_coor(table, row, col - 1)
            and not tab_coor(table, row - 1, col)
            and tab_coor(table, row, col + 1)
            and not tab_coor(table, row + 1, col)
        ):
            row -= 1
        # 6
        elif (
            not tab_coor(table, row, col - 1)
            and not tab_coor(table, row - 1, col)
            and tab_coor(table, row, col + 1)
            and tab_coor(table, row + 1, col)
        ):
            row -= 1
        # 7
        elif (
            not tab_coor(table, row, col - 1)
            and not tab_coor(table, row - 1, col)
            and not tab_coor(table, row, col + 1)
            and tab_coor(table, row + 1, col)
        ):
            col += 1
        # 8
        elif (
            tab_coor(table, row, col - 1)
            and not tab_coor(table, row - 1, col)
            and not tab_coor(table, row, col + 1)
            and tab_coor(table, row + 1, col)
        ):
            col += 1
        # 11
        elif (
            tab_coor(table, row, col - 1)
            and tab_coor(table, row - 1, col)
            and not tab_coor(table, row, col + 1)
            and not tab_coor(table, row + 1, col)
        ):
            row += 1
        # Pop first character from rest word
        word = word[1:]
    # Return filled table
    return table


if __name__ == "__main__":
    # print(spiral_matrix(2, "DOG"))
    # ["D", "O"],
    # ["+", "G"]
    # print(spiral_matrix(3, "COPYRIGHTS"))
    # ["G", "H", "T"],
    # ["I", "C", "O"],
    # ["R", "Y", "P"]
    # print(spiral_matrix(4, "SUPERLUMBERJACK"))
    # ["U", "M", "B", "E"],
    # ["L", "S", "U", "R"],
    # ["R", "E", "P", "J"],
    # ["+", "K", "C", "A"]
    # print(spiral_matrix(5, "SUPERLUMBERJACK"))
    # print(spiral_matrix(6, "SUPERLUMBERJACK"))
    # print(spiral_matrix(4, "EDABITTEROUS"))

    ### Edabit test
    import numpy as np

    np.testing.assert_equal(spiral_matrix(2, "DOG"), [["D", "O"], ["+", "G"]])

    np.testing.assert_equal(
        spiral_matrix(3, "COPYRIGHTS"),
        [["G", "H", "T"], ["I", "C", "O"], ["R", "Y", "P"]],
    )

    np.testing.assert_equal(
        spiral_matrix(3, "EDABITTER"),
        [["T", "E", "R"], ["T", "E", "D"], ["I", "B", "A"]],
    )

    np.testing.assert_equal(
        spiral_matrix(3, "EDABITTEROUS"),
        [["T", "E", "R"], ["T", "E", "D"], ["I", "B", "A"]],
    )

    np.testing.assert_equal(
        spiral_matrix(4, "SUPERLUMBERJACK"),
        [
            ["U", "M", "B", "E"],
            ["L", "S", "U", "R"],
            ["R", "E", "P", "J"],
            ["+", "K", "C", "A"],
        ],
    )

    np.testing.assert_equal(
        spiral_matrix(4, "EDABITTEROUS"),
        [
            ["T", "E", "R", "O"],
            ["T", "E", "D", "U"],
            ["I", "B", "A", "S"],
            ["+", "+", "+", "+"],
        ],
    )

    np.testing.assert_equal(
        spiral_matrix(5, "ABOUTLIFETHEUNIVERSEANDEVERYTHING"),
        [
            ["A", "N", "D", "E", "V"],
            ["E", "I", "F", "E", "T"],
            ["S", "L", "A", "B", "H"],
            ["R", "T", "U", "O", "E"],
            ["E", "V", "I", "N", "U"],
        ],
    )

    np.testing.assert_equal(
        spiral_matrix(5, "THEUNBEARABLESPIRAL"),
        [
            ["+", "+", "+", "+", "+"],
            ["+", "E", "A", "R", "A"],
            ["L", "B", "T", "H", "B"],
            ["A", "N", "U", "E", "L"],
            ["R", "I", "P", "S", "E"],
        ],
    )

    np.testing.assert_equal(
        spiral_matrix(6, "SHESELLSSHELLSBYTHESEASHOREWITHOUTASHELLSSELLINGLICENSE"),
        [
            ["E", "A", "S", "H", "O", "R"],
            ["S", "L", "S", "S", "H", "E"],
            ["E", "L", "S", "H", "E", "W"],
            ["H", "E", "S", "E", "L", "I"],
            ["T", "Y", "B", "S", "L", "T"],
            ["S", "A", "T", "U", "O", "H"],
        ],
    )