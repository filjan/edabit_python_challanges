"""
https://edabit.com/challenge/iN48LCvtsQFftc7L9

Word Search (Part 1)
This challenge involves finding words in an 8x8 grid. Given a string of 64
letters and a list of words to find, convert the string to an 8x8 list, and
return True if all words in the string can be found in the list. Return
False otherwise. Words can be read in any direction (horizontally,
vertically or diagonally).

Example
letters = "PSUWHATSLPACKAGENYOLRDVLFINGEZBMIREHQNJOATBVGYESJDUWUESTPSTICKEY"
words = ["stick", "most", "key", "vein", "yes", "package", "tube", "target",
"elm", "spy"]

This would give the list below:

[
  ["P", "S", "U", "W", "H", "A", "T", "S"],
  ["L", "P", "A", "C", "K", "A", "G", "E"],
  ["N", "Y", "O", "L", "R", "D", "V", "L"],
  ["F", "I", "N", "G", "E", "Z", "B", "M"],
  ["I", "R", "E", "H", "Q", "N", "J", "O"],
  ["A", "T", "B", "V", "G", "Y", "E", "S"],
  ["J", "D", "U", "W", "U", "E", "S", "T"],
  ["P", "S", "T", "I", "C", "K", "E", "Y"]
]
You would return True as all words can be found:

[
  ["_", "S", "_", "_", "_", "_", "T", "_"],
  ["_", "P", "A", "C", "K", "A", "G", "E"],
  ["N", "Y", "_", "_", "R", "_", "_", "L"],
  ["_", "I", "_", "G", "_", "_", "_", "M"],
  ["_", "_", "E", "_", "_", "_", "_", "O"],
  ["_", "T", "B", "V", "_", "Y", "E", "S"],
  ["_", "_", "U", "_", "_", "E", "_", "T"],
  ["_", "S", "T", "I", "C", "K", "_", "_"]
]
"""


def create_structured_string(strings):
    """
    Function returns list of string as extended copy of given in reversed order
    """
    to_extend_string = strings.copy()
    to_extend_string.extend([rev[::-1] for rev in to_extend_string])
    return to_extend_string


def find_horizontal_sequences(letters):
    """
    Functions returns all possible horizontal strings (reversed in order also)
    from 8x8 letter-board made of letters
    """
    # List of horizontals rows read from left to right
    horizontals_left = [letters[num : num + 8] for num in range(0, len(letters), 8)]
    # List of horizontal strings (read from left to right and as reversed)
    horizontals_both = create_structured_string(horizontals_left)
    return horizontals_both


def find_vertical_sequences(letters):
    """
    Functions returns all possible vertical strings (reversed in order also)
    from 8x8 letter-board made of letters
    """
    # List of vertical columns read from top to down
    verticals_top = [letters[num::8] for num in range(0, len(letters) // 8)]
    # List of vertical columns (read from top to down and as reversed)
    verticals_both = create_structured_string(verticals_top)
    return verticals_both


def find_diagonal_sequences(letters):
    """
    Index diagonal looks like (within each iteration):

        ascending:
        0
        1,8
        2,9,16
        3,10,17,24
        and so on until half

        descending:
        7
        6,15
        5,14,23
        4,13,22,31
        and so on until half

    Functions uses same indexes for reversed letters, to include second half
    of potential letter-board.

    """
    # List of ascending like indexes of diagonals (looking from left)
    index_diagonal_asc = []
    # List of descending like indexes of diagonals (looking from left)
    index_diagonal_des = []
    # Container to each possible diagonal
    diagonals = []
    # Iteration through 8 (dimension of square side describing letter-board)
    for ind in range(len(letters) // 8):
        # Update indexes of ascending diagonals
        index_diagonal_asc.append(ind * 8)
        # Update indexes of descending diagonals
        index_diagonal_des.append((ind + 1) * 8 - 1)
        #
        # Diagonals from top-left
        diag_t_l = "".join([letters[i] for i in index_diagonal_asc])
        # Diagonals from down-right
        diag_d_r = "".join([letters[::-1][i] for i in index_diagonal_asc])
        # Diagonals from top-right
        diag_t_r = "".join([letters[i] for i in index_diagonal_des])
        # Diagonals from down-left
        diag_d_l = "".join([letters[::-1][i] for i in index_diagonal_des])
        #
        # Update diagonals container with new diagonal strings
        diagonals.extend([diag_t_l, diag_d_r, diag_t_r, diag_d_l])
        # Update diagonals for next iteration :
        # Increase each component by one
        index_diagonal_asc = list(map(lambda x: x + 1, index_diagonal_asc))
        # Decrease each component by one
        index_diagonal_des = list(map(lambda x: x - 1, index_diagonal_des))
    # Create reversed strings and put into container
    diagonals_both = create_structured_string(diagonals)
    #
    return diagonals_both


def word_search(letters, words):
    """
    Function
    """
    possible_strings = set(
        find_horizontal_sequences(letters)
        + find_vertical_sequences(letters)
        + find_diagonal_sequences(letters)
    )
    # Map given words to be consistent with letters (each letter as uppercase)
    words = map(lambda x: x.upper(), words)
    # Iterate over searching words, and return False if only one is absent
    for check_word in words:
        if not any(filter(lambda x: check_word in x, possible_strings)):
            return False
    return True


if __name__ == "__main__":
    # letters = "KARALLOCGNTEBMAIBHEOCPFMRUNEOFUMAWGKDURAVDRECAZSEMJALOTSOXVACUNU"
    # words = [
    #     "mass",
    #     "cap",
    #     "brave",
    #     "knee",
    #     "collar",
    #     "alarm",
    #     "vacuum",
    #     "leg",
    #     "fur",
    #     "lot",
    # ]

    # letter_board = [
    #     [letter for letter in letters[num : num + 8]]
    #     for num in range(0, len(letters), 8)
    # ]
    # [print(row) for row in letter_board]

    import numpy as np

    # My tests
    np.testing.assert_equal(
        word_search(
            "PSUWHATSLPACKAGENYOLRDVLFINGEZBMIREHQNJOATBVGYESJDUWUESTPSTICKEY",
            [
                "stick",
                "most",
                "key",
                "vein",
                "yes",
                "package",
                "tube",
                "target",
                "elm",
                "spy",
            ],
        ),
        True,
    )
    # Edabit tests
    np.testing.assert_equal(
        word_search(
            "ALLESSAYIOPEBUTNRACANHHEHREFOXIAAERUIFNRPRGELTHXSHWTUOHSTCURLITE",
            [
                "thin",
                "oil",
                "tube",
                "fox",
                "thought",
                "curl",
                "air",
                "essay",
                "shout",
                "era",
            ],
        ),
        True,
    )
    np.testing.assert_equal(
        word_search(
            "DELDNUBPETENHARALOSSOUNTPCHIEFDRUROHSOTZOAWENINLCLPOKERFORBIDMTC",
            [
                "crisis",
                "kit",
                "pat",
                "chief",
                "show",
                "poker",
                "forbid",
                "couple",
                "donor",
                "bundle",
            ],
        ),
        False,
    )
    np.testing.assert_equal(
        word_search(
            "LOBMYSALORDOTRECENTENSIOXAUEDNDYLHSAMTLFVEINNLBLSECOACERTYMRBFEH",
            [
                "nuance",
                "record",
                "helmet",
                "rally",
                "parade",
                "tension",
                "symbol",
                "separate",
                "vein",
                "ash",
            ],
        ),
        False,
    )
    np.testing.assert_equal(
        word_search(
            "PEANUTIHATEFOUSTLAGBRYUGAGAOANSNCVSTXENEEISTSDERNUELHOCTTAMEPROS",
            [
                "gate",
                "bee",
                "bottle",
                "deny",
                "census",
                "peanut",
                "message",
                "palace",
                "next",
                "strength",
            ],
        ),
        True,
    )
    np.testing.assert_equal(
        word_search(
            "CAFZEROTEOALGWNNGSNEAECIIZPCDGFSTTDIEBFUFASCEINOIELNURVCRHTXGMCE",
            [
                "ice",
                "cousin",
                "resident",
                "conceive",
                "drift",
                "heat",
                "zero",
                "flag",
                "run",
                "pierce",
            ],
        ),
        False,
    )
    np.testing.assert_equal(
        word_search(
            "KARALLOCGNTEBMAIBHEOCPFMRUNEOFUMAWGKDURAVDRECAZSEMJALOTSOXVACUNU",
            [
                "mass",
                "cap",
                "brave",
                "knee",
                "collar",
                "alarm",
                "vacuum",
                "leg",
                "fur",
                "lot",
            ],
        ),
        True,
    )
    np.testing.assert_equal(
        word_search(
            "NYBNETADOORTNAOESAYHOPDLITRSEOWIRSHEWHRVPOGWOSNEFAMILIARTENESBHT",
            [
                "ant",
                "age",
                "familiar",
                "net",
                "root",
                "say",
                "prison",
                "bishop",
                "deliver",
                "slow",
            ],
        ),
        True,
    )
    np.testing.assert_equal(
        word_search(
            "HOAIKPMKWDBNSEECIEESAHMAZFDTCRBBCIRIAUEDANONPSRETEECSHOECRTTTSIF",
            [
                "debate",
                "ask",
                "member",
                "rush",
                "obscure",
                "instinct",
                "feed",
                "fist",
                "all",
                "catch",
            ],
        ),
        False,
    )
    np.testing.assert_equal(
        word_search(
            "EBEGLAEROZPOSTVLKYEEONEOCUTETEISIBUYRCHTRMYGSBCNTCERIDAPRDIKOMAN",
            [
                "buy",
                "lost",
                "real",
                "breeze",
                "direct",
                "man",
                "post",
                "trick",
                "degree",
                "achieve",
            ],
        ),
        True,
    )
    np.testing.assert_equal(
        word_search(
            "MITSFOCAERCULTURMDHYKIRTGLANCELMYPRODUCEODICSVLGLETSOOADPRYGHMNI",
            [
                "produce",
                "charity",
                "employ",
                "art",
                "gem",
                "raid",
                "glance",
                "hole",
                "moon",
                "ear",
            ],
        ),
        False,
    )