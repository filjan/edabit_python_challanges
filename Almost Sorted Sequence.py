"""
https://edabit.com/challenge/HBuWYyh5YCmDKF4uH
Almost Sorted Sequence
An almost-sorted sequence is a sequence that is strictly increasing or
strictly decreasing if you remove a single element from the list (no more,
no less). Write a function that returns True if a list is almost-sorted,
and False otherwise.

For example, if you remove 80 from the first example, it is perfectly
sorted in ascending order. Similarly, if you remove 7 from the second example,
it is perfectly sorted in descending order.

Examples
almost_sorted([1, 3, 5, 9, 11, 80, 15, 33, 37, 41] ) ➞ True

almost_sorted([6, 5, 4, 7, 3]) ➞ True

almost_sorted([6, 4, 2, 0]) ➞ False
// Sequence is already sorted.

almost_sorted([7, 8, 9, 3, 10, 11, 12, 2]) ➞ False
// Requires removal of more than 1 item.
Notes
Completely sorted lists should return False.
Lists will always be > 3 in length (to remove ambiguity).
Numbers in each input list will be unique - don't worry about "ties".
"""


def almost_sorted(list_of_numbers):
    """
    Function returns True if list of numbers is almost sorted
    Input assumptions states that numbers are unique, so condition defining
    descendence is opposition to ascendance.
    """

    # Define sequence of indices of components in argument (preclude last index)
    range_of_numbers_idxs = range(len(list_of_numbers))

    # Define ascending boolean condition of two consecutive values
    condition_ascending = lambda x: list_of_numbers[x + 1] > list_of_numbers[x]

    # Define list of bools comparing each consecutive pair
    bool_ascending_components = list(
        map(condition_ascending, range_of_numbers_idxs[:-1])
    )
    ## Count number of violations in assessing ascendence
    counter_falsities = bool_ascending_components.count(False)
    counter_truths = bool_ascending_components.count(True)
    first_bool = bool_ascending_components[0]
    last_bool = bool_ascending_components[-1]
    ## Condition when first or last component violates conditions
    # (there is no necessity to check separate sublists)
    if counter_falsities == 1 and (first_bool == False or last_bool == False):
        return True
    elif counter_truths == 1 and (first_bool == True or last_bool == True):
        return True
    ## Condition when redundant figure is inside list (but not at sides)
    # Copy input argument
    list_of_numbers_copy = list_of_numbers
    ## If there is more than one falsities or truths -> return False
    ## In other cases make more demanded investigation
    # Check if list is almost ascendent
    if counter_falsities == 1:
        violation_idx = bool_ascending_components.index(False)
        # violation_idx is created based on pair of values, doesn't point violating value
        if list_of_numbers[violation_idx - 1] > list_of_numbers[violation_idx + 1]:
            violation_idx += 1
        else:
            pass
        # Check if list is not only piece-wisely almost sorted
        list_of_numbers_copy.pop(violation_idx)
        if list_of_numbers_copy == sorted(list_of_numbers_copy):
            return True
        else:
            return False
    # Check if list is almost descendent
    if counter_truths == 1:
        violation_idx = bool_ascending_components.index(True)
        # violation_idx is created based on pair of values, doesn't point violating value
        if list_of_numbers[violation_idx - 1] > list_of_numbers[violation_idx + 1]:
            pass
        else:
            violation_idx += 1
        # Check if list is not only piece-wisely almost sorted
        list_of_numbers_copy.pop(violation_idx)
        if list_of_numbers_copy == sorted(list_of_numbers_copy, reverse=True):
            return True
        else:
            return False

    return False


if __name__ == "__main__":

    import numpy as np

    if True:
        ## My asserts:
        np.testing.assert_equal(almost_sorted([0, 2, 4, 6, 1, 3, 5]), False)
        np.testing.assert_equal(almost_sorted([-1, 3, 2, 1]), True)
        np.testing.assert_equal(almost_sorted([3, 2, 1, 5]), True)

        np.testing.assert_equal(almost_sorted([3, -1, 2, 1]), True)
        np.testing.assert_equal(almost_sorted([3, 2, 5, 1]), True)

        ## Edabit asserts:
        np.testing.assert_equal(
            almost_sorted([1, 3, 5, 9, 11, 80, 15, 33, 37, 41]),
            True,
            "remove 80 should work",
        )
        np.testing.assert_equal(
            almost_sorted([6, 5, 4, 7, 3]), True, "remove 7 should work"
        )
        np.testing.assert_equal(
            almost_sorted([6, 4, 2, 0]),
            False,
            "numbers should not be completely sorted",
        )
        np.testing.assert_equal(almost_sorted([7, 8, 9, 3, 10, 11, 12, 2]), False)
        np.testing.assert_equal(
            almost_sorted([9, 1, 8, 2]), True, "remove 1 should work"
        )
        np.testing.assert_equal(
            almost_sorted([1, 3, 9, 44, 15, 17, 33]), True, "remove 44 should work"
        )
        np.testing.assert_equal(
            almost_sorted([5, 4, 3, 2, -1, 0]), True, "remove -1 should work"
        )
        np.testing.assert_equal(
            almost_sorted([5, 2, 3, 4]), True, "remove 5 should work"
        )
        np.testing.assert_equal(almost_sorted([8, 3, 7, 4, 9]), False)
        np.testing.assert_equal(
            almost_sorted([-3, -4, -5, -7]),
            False,
            "numbers should not be completely sorted",
        )
        np.testing.assert_equal(
            almost_sorted([5, 6, 7, 8]), False, "numbers should not be completed sorted"
        )
        np.testing.assert_equal(almost_sorted([9, 1, 8, 2, 7, 3]), False)