"""
https://edabit.com/challenge/i747Wtc7pCukr8GRC
Maximum and Minimum Product Triplets
Write two functions:

One that returns the maximum product of three numbers in a list.
One that returns the minimum product of three numbers in a list.
Examples
max_product([-8, -9, 1, 2, 7]) ➞ 504

max_product([-8, 1, 2, 7, 9]) ➞ 126

min_product([1, -1, 1, 1]) ➞ -1

min_product([-5, -3, -1, 0, 4]) ➞ -15
Notes
N/A
"""


def multiply_triplets(list_of_nums, ascending):
    """
    Function return two products of multiplications.
    Sufficed with _1 are the first 3 values from sorted list.
    Sufficed with _2 are the first 1 valuea and 2 lasts from sorted list.
    """
    list_of_nums.sort(reverse=ascending)
    multiplication_1 = list_of_nums[0] * list_of_nums[1] * list_of_nums[2]
    multiplication_2 = list_of_nums[0] * list_of_nums[-1] * list_of_nums[-2]
    return multiplication_1, multiplication_2


def max_product(list_of_nums):
    """
    Function returns maximum product of three values picked from argument list
    """
    product_1, product_2 = multiply_triplets(list_of_nums, ascending=True)
    return max([product_1, product_2])


def min_product(list_of_nums):
    """
    Function returns minimum product of three values picked from argument list
    """
    product_1, product_2 = multiply_triplets(list_of_nums, ascending=False)
    return min([product_1, product_2])


if __name__ == "__main__":
    import numpy as np

    # max_product
    np.testing.assert_equal(max_product([1, -1, 1]), -1)
    np.testing.assert_equal(max_product([1, -1, 1, 1]), 1)
    np.testing.assert_equal(max_product([-8, -9, 1, 2, 7]), 504)
    np.testing.assert_equal(max_product([-8, 1, 2, 7, 9]), 126)
    np.testing.assert_equal(max_product([1, 1, 5, 1, 1, -10, -1]), 50)
    np.testing.assert_equal(max_product([-8, -7, -6, -5]), -210)
    np.testing.assert_equal(max_product([-8, -7, -6, -5, 1]), 56)
    np.testing.assert_equal(max_product([1, 0, 1, 0, 0]), 0)
    np.testing.assert_equal(max_product([-5, 1, 10, 0, 0]), 0)
    np.testing.assert_equal(max_product([-5, -1, -1, 0, 0]), 0)
    np.testing.assert_equal(max_product([-5, 1, -1, 0, 0]), 5)
    np.testing.assert_equal(max_product([-5, -3, -1, 0, 4]), 60)
    np.testing.assert_equal(max_product([5, 3, -1, 0, -4, 7, 7, 9]), 441)
    # min_product
    np.testing.assert_equal(min_product([1, -1, 1]), -1)
    np.testing.assert_equal(min_product([1, -1, 1, 1]), -1)
    np.testing.assert_equal(min_product([-8, -9, 1, 2, 7]), -126)
    np.testing.assert_equal(min_product([-8, 1, 2, 7, 9]), -504)
    np.testing.assert_equal(min_product([1, 1, 5, 1, 1, -10, -1]), -50)
    np.testing.assert_equal(min_product([-8, -7, -6, -5]), -336)
    np.testing.assert_equal(min_product([-8, -7, -6, -5, 1]), -336)
    np.testing.assert_equal(min_product([1, 0, 1, 0, 0]), 0)
    np.testing.assert_equal(min_product([-5, 1, 10, 0, 0]), -50)
    np.testing.assert_equal(min_product([-5, -1, -1, 0, 0]), -5)
    np.testing.assert_equal(min_product([-5, 1, -1, 0, 0]), 0)
    np.testing.assert_equal(min_product([-5, -3, -1, 0, 4]), -15)
    np.testing.assert_equal(min_product([5, 3, -1, 0, -4, 7, 7, 9]), -252)