"""
https://edabit.com/challenge/tRHaoWNaHBJCYD5Nx
Same Letter Patterns
Create a function that returns True if two strings share the same letter
pattern, and False otherwise.

Examples
same_letter_pattern("ABAB", "CDCD") ➞ True

same_letter_pattern("ABCBA", "BCDCB") ➞ True

same_letter_pattern("FFGG", "CDCD") ➞ False

same_letter_pattern("FFFF", "ABCD") ➞ False
Notes
N/A
"""


def same_letter_pattern(string_1, string_2):
    """ Function compares pattern between argument strings"""
    # Extract uniques characters
    uniques_1 = set(string_1)
    uniques_2 = set(string_2)
    # First mandatory condition
    if len(uniques_1) != len(uniques_2):
        return False
    # Declaration lists containing matches
    patterns_1 = []
    patterns_2 = []
    # Create lists of matches between unique characters and strings
    for unique_1, unique_2 in zip(uniques_1, uniques_2):
        patterns_1.append(tuple(map(lambda x: x == unique_1, string_1)))
        patterns_2.append(tuple(map(lambda x: x == unique_2, string_2)))
    # Sufficient condition to compare patterns and assess as same
    list_of_matches = list(map(lambda x: x in patterns_1, patterns_2))
    return all(list_of_matches)


if __name__ == "__main__":
    import numpy as np
    np.testing.assert_equal(same_letter_pattern('ABAB', 'CDCD'), True)
    np.testing.assert_equal(same_letter_pattern('AAABBB', 'CCCDDD'), True)
    np.testing.assert_equal(same_letter_pattern('ABCBA', 'BCDCB'), True)
    np.testing.assert_equal(same_letter_pattern('AAAA', 'BBBB'), True)
    np.testing.assert_equal(same_letter_pattern('BAAB', 'ABBA'), True)
    np.testing.assert_equal(same_letter_pattern('BAAB', 'QZZQ'), True)
    np.testing.assert_equal(same_letter_pattern('TTZZVV', 'PPSSBB'), True)
    np.testing.assert_equal(same_letter_pattern('ZYX', 'ABC'), True)
    np.testing.assert_equal(same_letter_pattern('AABAA', 'SSCSS'), True)
    np.testing.assert_equal(same_letter_pattern('AABAABAA', 'SSCSSCSS'), True)
    np.testing.assert_equal(same_letter_pattern('UBUBUBUB', 'WEWEWEWE'), True)
    np.testing.assert_equal(same_letter_pattern('FFGG', 'FFG'), False)
    np.testing.assert_equal(same_letter_pattern('FFGG', 'CDCD'), False)
    np.testing.assert_equal(same_letter_pattern('FFFG', 'GGHI'), False)
    np.testing.assert_equal(same_letter_pattern('FFFF', 'ABCD'), False)
    np.testing.assert_equal(same_letter_pattern('ABCA', 'ABCD'), False)
    np.testing.assert_equal(same_letter_pattern('ABCAAA', 'DDABCD'), False)