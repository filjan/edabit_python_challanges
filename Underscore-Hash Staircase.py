"""
https://edabit.com/challenge/YqLBEZJR9ySndYQpH

Underscore-Hash Staircase
Create a function that will build a staircase using the underscore _ and hash
# symbols. A positive value denotes the staircase's upward direction and
downwards for a negative value.

Examples
staircase(3) ➞ "__#\n_##\n###"
__#
_##
###

staircase(7) ➞ "______#\n_____##\n____###\n___####\n__#####\n_######\n#######"
______#
_____##
____###
___####
__#####
_######
#######

staircase(2) ➞ "_#\n##"
_#
##

staircase(-8) ➞ "########\n_#######\n__######\n___#####\n____####\n_____###\n______##\n_______#"
########
_#######
__######
___#####
____####
_____###
______##
_______#
Notes
All inputs are either positive or negative values.
The string to be returned is adjoined with the newline character (\n).
A recursive version of this challenge can be found in here.

"""


def update_n(n):
    """
    Function:
        - decreases n by 1 when its positive
        or
        - increase n by 1 when negative
    """
    if n < 0:
        n += 1
    elif n > 0:
        n -= 1
    else:
        raise Exception("n can't be 0 during calling update_n")
    return n


def define_num_of_sharps_unders(n, stairs):
    """
    Function returns tuple of two string: underscors and sharps. Each has
    proper length to join them into one string (as row)
    """
    # Determine width of the staircase's board
    if n < 0:
        width = len(stairs[0])
    elif n > 0:
        width = len(stairs[-1])
    # Number of sharps, underscores is independent to sign in front of n
    sharps = (width - abs(n)) * "_"
    unders = abs(n) * "#"
    #
    return sharps, unders


def append_to_stairs(n, new_line_characters, stairs):
    """
    Function joins undercores and sharps into one string and append it to
    stairs list, in proper way:
        - if n is negative: join string into end of list
        - if n is positive: join string into beginning of the list
    """
    new_line_string = "".join(new_line_characters)
    if n < 0:
        stairs.append(new_line_string)
    elif n > 0:
        stairs.insert(0, new_line_string)


def staircase(n):
    """
    Function create create stairs according to argument n.
    """
    # Initialize stairs list
    stairs = [abs(n) * "#"]
    # Update n
    n = update_n(n)
    # Iterate through n
    while n != 0:
        # Define number of # and _
        new_line_characters = define_num_of_sharps_unders(n, stairs)
        # Append appropriately characters to stairs
        append_to_stairs(n, new_line_characters, stairs)
        # Update n
        n = update_n(n)
    # Return string (convert stairs list into string separated with newline)
    return "\n".join(stairs)


if __name__ == "__main__":
    a = staircase(1)
    print(f"ret : \n{a}")

    b = staircase(-2)
    print(f"ret : \n{b}")

    c = staircase(3)
    print(f"ret : \n{c}")

    d = staircase(-3)
    print(f"ret : \n{d}")

    import numpy as np

    ## Edabit
    num_vector = [3, 7, 2, -8, 4, -12, 11, -6]
    res_vector = [
        "__#\n_##\n###",
        "______#\n_____##\n____###\n___####\n__#####\n_######\n#######",
        "_#\n##",
        "########\n_#######\n__######\n___#####\n____####\n_____###\n______##\n_______#",
        "___#\n__##\n_###\n####",
        "############\n_###########\n__##########\n___#########\n____########\n_____#######\n______######\n_______#####\n________####\n_________###\n__________##\n___________#",
        "__________#\n_________##\n________###\n_______####\n______#####\n_____######\n____#######\n___########\n__#########\n_##########\n###########",
        "######\n_#####\n__####\n___###\n____##\n_____#",
    ]
    for i, x in enumerate(num_vector):
        np.testing.assert_equal(staircase(x), res_vector[i])