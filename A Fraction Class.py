"""
https://edabit.com/challenge/JZfyYujftavv6APLs
A Fraction Class

Mathematically, a fraction can be defined as the ratio a/b, where a and b are
integers and b is non zero.

In this challenge, we are going to implement a Fraction class and some methods
to support arithmetic and comparison operations on them. Write a Fraction
class that meets the following requirements:

- Initialisation: Create a Fraction object with a as numerator and b as
denominator, e.g. Fraction(5, 6) where a = 5 and b = 6. Validate that a and b
are both integers and b is non-zero. If not, print the error message shown
below and do not create any variables. The numerator and denominator should
also be stored in factorized form, e.g. 4/6 should be stored as 2/3.

- Representation: It should be possible to print a Fraction or obtain a string
representation of it via the str built-in function. For a positive fraction,
this should return the string "x/y" e.g. "5/4". If the fraction is negative,
return "- x/y" e.g. "- 5/9" (note the space after the - sign). If the fraction
has failed to initialize, detect this (it shouldn't have any instance
variables) and return the string "Initialisation Failed".

- Arithmetic Operations: Implement suitable methods to support +, -, *, and
/ operations between two fractions with the usual arithmetic meaning. Return
the appropriate factorised Fraction e.g Fraction(2, 3) * Fraction(2, 5) should
return Fraction(4, 15). A division operation could result in a zero
denominator and this should be catered for by printing the error message
described in the initialization section and returning None. If the result of
an operation is a negative fraction, return the numerator as the negative
integer.

- Comparison Operations: Implement suitable methods to compare two Fraction
objects using operators ==,!=, <, >, <= and >= with the usual meanings.
For ==, apply a tolerance factor of 10^-7.

- Instance Method: Implement instance method decimal. This should return the
decimal equivalent of the Fraction object to up to seven decimal places e.g.
Fraction(1/4).decimal() -> 0.25.

- Class Method: Implement class method fraction(decimal) which returns a
Fraction object equivalent to decimal to up to seven decimal places e.g.
Fraction.fraction(2.5) -> Fraction(5, 2). If the parameter decimal is an
integer, set the denominator in Fraction to 1.

Examples
str(Fraction(2, -3)) ➞ ""- 2/3"

str(Fraction(3, 0)) ➞ "Initialisation Failed"
# Should print "numerator must be an integer and denominator a non zero integer".

Fraction(1, 5) + Fraction(3, 10) ➞ Fraction(1, 2)

Fraction(5, 2) * Fraction(6, -10) ➞ Fraction(-3, 2)

Fraction(0, 3) / Fraction(0, 5) ➞ None
# Should print "numerator must be an integer and denominator a non zero integer".

Fraction(-4, 5) > Fraction(-3, 5) ➞ False

Fraction(10, 9) < Fraction(2, 1) ➞ True

Fraction.fraction(11) ➞ Fraction(11, 1)

Fraction(5, 9).decimal() ➞ 0.5555556
Notes
You will need to use a technique called operator overloading to successfully
complete this challenge. This requires the use of what are termed magic or
dunder methods in Python (Resources have a useful link for these). If you need
to brush up on arithmetic with fractions, there's a link for that too.

"""

import functools

def prepare_for_math_operation(fun):
    """
    Decorator converts two given Fraction instances into common denominator,
    and changes nominator+denominator appropriately.
    Second functionality is calling simplifying returning Fraction, into the
    least possible denominator.
    """

    @functools.wraps(fun)
    def wrapper(self, other):
        # Copy denominators to local variables
        original_denominator_first = self._b
        original_denominator_second = other._b
        # Multiply first nominator and denominator by second denominator
        self._a *= original_denominator_second
        self._b *= original_denominator_second
        # Multiply second nominator and denominator by first denominator
        other._a *= original_denominator_first
        other._b *= original_denominator_first
        # Call decorated function and assign result to variable
        result = fun(self, other)
        # Simplify independently two given Fraction
        self.simplify()
        other.simplify()
        # If returned value is bool or None -> return value immediately. If
        # returned value if Fraction, than simplify him
        if isinstance(result, Fraction):
            result.simplify()
        return result

    return wrapper


class Fraction:
    """
    Class is implementation of fraction (so called 'common', 'vulgar', 'simple')
    """

    def __init__(self, a, b):
        """
        Define local variables:
            _a - nominator (absolute value)
            _b - denominator (absolute value)
            _sign - '-1' when fraction is negative and '1' positive
        During __init__ method check_init checks correctness of given input.
        """
        # Call checking of given values
        self.check_init = self.check_init_input(a, b)
        # if check is correct, assign input to attributes, if no assign None
        if self.check_init[0]:
            # Numerator
            self._a = abs(a)
            # Denominator
            self._b = abs(b)
            # Sign
            if a == 0 or b == 0:
                self._sign = 1
            else:
                self._sign = (a * b) / (abs(a * b))
        else:
            self._a = None
            self._b = None
            self._sign = None

    @staticmethod
    def check_init_input(a, b):
        """
        Method checks correctness of input values during __init__.
        Method return tuple with values:
            first - False or True according to faulty input
            second - additional note
        """
        # Check if values are integers
        if not isinstance(a, int) or not isinstance(b, int):
            return (False, "Initialisation Failed")
        # Check if b (denominator to be) is not 0
        if b == 0:
            print("numerator must be an integer and denominator a non zero integer")
            return (False, "Initialisation Failed")
        # In the end return True
        return (True, "Initialisation Succeded")

    @staticmethod
    def find_common_dividers(fig_1, fig_2):
        """
        Function generates values of common integer divider of given pair of figures
        """
        # Convert given values to absolute values
        fig_1, fig_2 = abs(fig_1), abs(fig_2)
        # Input values could be 0, and then assume that common divider is 1
        if fig_1 == 0 or fig_2 == 0:
            yield 1
        # Iterate to minimum value between given values (increased by one, because
        # range take into account last value exclusively)
        for divider in range(1, min(abs(fig_1 + 1), abs(fig_2)) + 1):
            # divider is THE divider only when each value is modulo dividing
            if fig_1 % divider == 0 and fig_2 % divider == 0:
                yield divider

    @classmethod
    def fraction(cls, float_value):
        """
        Method takes float (or int) and returns it to Fraction
        """
        # Check if given value is float or int
        if not isinstance(float_value, float) and not isinstance(float_value, int):
            print("Fraction method require float or int input")
            return None
        # If given value is int, convert it to float
        if isinstance(float_value, int):
            float_value = float(float_value)
        ## Decomposition of float:
        # Extract unit values
        units_part = int(float_value // 1)
        # Extract fractional value as float
        fraction_part = round(float_value % 1, 7)
        # Cut "0."
        fraction_part = int(str(fraction_part)[2:])
        # Define denominator of prospective fraction, as depth of 10ths
        denominator = 10 ** (len(str(fraction_part)))
        # Define nominator with base of denominator
        nominator = units_part * denominator + fraction_part
        # Create new fraction
        new_frac = Fraction(nominator, denominator)
        # Simplify common fraction e.g. 0.5 -> 5/10 -> 1/2
        new_frac.simplify()
        # Return new fraction
        return new_frac

    def decimal(self):
        """
        Return float represantation of fraction
        """
        float_value = round(self._sign * (self._a / self._b), 7)
        return float_value

    def simplify(self):
        """
        Overwrite nominator and denominator as values divided by them maximal
        common divider
        """
        common_max_divider = max(self.find_common_dividers(self._a, self._b))
        self._a = int(self._a / common_max_divider)
        self._b = int(self._b / common_max_divider)

    def __str__(self):
        """
        String represanetion
        """
        # Special message if initialization failed
        if not self.check_init[0]:
            return self.check_init[1]
        # Two ways of representation acc. to positive/negative value
        if self._sign < 0:
            return_string = f"- {self._a}/{self._b}"
        else:
            return_string = f"{self._a}/{self._b}"
        return return_string

    def __repr__(self):
        """
        Same as __str__
        """
        return self.__str__()

    @prepare_for_math_operation
    def __add__(self, other):
        """
        Create new Fraction instance with added nominators, with common
        denominator.
        """
        numerator = int(self._a * self._sign + other._a * other._sign)
        denominator = self._b
        result = Fraction(numerator, denominator)
        if not result.check_init[0]:
            return_value = None
        else:
            return_value = result
        return return_value

    @prepare_for_math_operation
    def __sub__(self, other):
        """
        Create new Fraction instance with subtracted nominators, with common
        denominator.
        """
        numerator = int(self._a * self._sign - other._a * other._sign)
        denominator = self._b
        result = Fraction(numerator, denominator)
        if not result.check_init[0]:
            return_value = None
        else:
            return_value = result
        return return_value

    @prepare_for_math_operation
    def __mul__(self, other):
        """
        Create new Fraction instance with multiplied nominators and
        denominators.
        """
        numerator = int(self._a * self._sign * other._a * other._sign)
        denominator = self._b * other._b
        result = Fraction(numerator, denominator)
        if not result.check_init[0]:
            return_value = None
        else:
            return_value = result
        return return_value

    @prepare_for_math_operation
    def __truediv__(self, other):
        """
        Create new Fraction instance with cross multiplication between
        nominator-denominator
        """
        numerator = int(self._a * self._sign * other._b * other._sign)
        denominator = self._b * other._a
        result = Fraction(numerator, denominator)
        if not result.check_init[0]:
            return_value = None
        else:
            return_value = result
        return return_value

    @prepare_for_math_operation
    def __eq__(self, other):
        """
        Return True if signs AND nominators are same
        """
        if not self.check_init[0]:
            return_value = None
        else:
            return_value = bool(self._a == other._a and self._sign == other._sign)
        return return_value

    @prepare_for_math_operation
    def __ne__(self, other):
        """
        Return True if signs OR nominators are different
        """
        if not self.check_init[0]:
            return_value = None
        else:
            return_value = bool(self._a != other._a or self._sign != other._sign)
        return return_value

    @prepare_for_math_operation
    def __gt__(self, other):
        """
        Return True if nominators(with signs) fulfil condition
        """
        return_value = bool(self._sign * self._a > other._sign * other._a)
        return return_value

    @prepare_for_math_operation
    def __ge__(self, other):
        """
        Return True if nominators(with signs) fulfil condition
        """
        return_value = bool(self._sign * self._a >= other._sign * other._a)
        return return_value

    @prepare_for_math_operation
    def __lt__(self, other):
        """
        Return True if nominators(with signs) fulfil condition
        """
        return_value = bool(self._sign * self._a < other._sign * other._a)
        return return_value

    @prepare_for_math_operation
    def __le__(self, other):
        """
        Return True if nominators(with signs) fulfil condition
        """
        return_value = bool(self._sign * self._a <= other._sign * other._a)
        return return_value


if __name__ == "__main__":

    import numpy as np

    fra_1 = Fraction(10, 9)
    fra_2 = Fraction(2, 1)

    ## My tests
    f_1 = Fraction(-1, 1) + Fraction(-1, 1)
    f_1 = Fraction(-1, 1) * Fraction(0, 1)

    np.testing.assert_equal(str(Fraction(2, -3)), "- 2/3")
    np.testing.assert_equal(str(Fraction(3, 0)), "Initialisation Failed")
    np.testing.assert_equal(Fraction(1, 5) + Fraction(3, 10), Fraction(1, 2))
    np.testing.assert_equal(Fraction(5, 2) * Fraction(6, -10), Fraction(-3, 2))
    np.testing.assert_equal(Fraction(0, 3) / Fraction(0, 5), None)
    np.testing.assert_equal(Fraction(-4, 5) > Fraction(-3, 5), False)
    np.testing.assert_equal(Fraction(10, 9) < Fraction(2, 1), True)

    np.testing.assert_equal(Fraction.fraction(11), Fraction(11, 1))
    np.testing.assert_equal(Fraction.fraction(2.5), Fraction(5, 2))
    np.testing.assert_equal(Fraction(5, 9).decimal(), 0.5555556)

    ## Edabit tests
    a = Fraction(-1, 2)
    b = Fraction(3, 6)
    c = Fraction(1, 4)
    tests = [
        ("a + a", "Fraction(-1,1)"),
        ("str(Fraction(1,2))", "'1/2'"),
        ("str(Fraction(2,0))", "'Initialisation Failed'"),
        ("a - b - c", "Fraction(-5,4)"),
        ("Fraction(-2,6) - Fraction(1,3)", "Fraction(-2,3)"),
        ("Fraction(-2,3) * Fraction(3,4)", "Fraction(-1,2)"),
        ("Fraction(-2,3) / Fraction(8,6)", "Fraction(-1,2)"),
        ("Fraction(-2,-3) + Fraction(-8,-4)", "Fraction(8,3)"),
        ("Fraction(9,5) * Fraction(10,18)", "Fraction(1,1)"),
        ("Fraction(0,2) / Fraction(0,2)", "None"),
        ("Fraction.fraction(2.0)", "Fraction(2,1)"),
        ("Fraction.fraction(2.9145)", "Fraction(5829,2000)"),
        ("c.decimal()", "0.25"),
        ("Fraction(5,9).decimal()", "0.5555556"),
        ("Fraction(22,7).decimal()", "3.1428571"),
        ("Fraction(1,3) ==  Fraction(2,6)", "True"),
        ("Fraction(1,3) != Fraction(-2,6)", "True"),
        ("Fraction(-4,5) > Fraction(-3,5)", "False"),
        ("Fraction(-3,5) < Fraction(-4,5)", "False"),
        ("Fraction(-4,5) <= Fraction(-3,5)", "True"),
        ("Fraction(-3,5) >= Fraction(-4,5)", "True"),
        ("str(Fraction(3,-7))", "'- 3/7'"),
        ("str(Fraction(-5,-9))", "'5/9'"),
    ]

    for test in tests:
        print(test)
        np.testing.assert_equal(eval(test[0]), eval(test[1]))
        print("ok")