"""
https://edabit.com/challenge/FPNLQWdiShE7HsFki

Spider🕷️vs. 🦟 Fly
A spider web is defined by rings numbered from 0-4 from the center and radials
labeled clock-wise from the top as A-H.

Create a function that takes the coordinates of spider and fly and returns the
shortest path for the spider to get to the fly.

It's worth noting that the shortest path should be calculated "geometrically",
not by counting the number of points that path goes through.
We could arrange that:

Angle between every pair of radials is the same (45 degrees).
Distance between every pair of rings is always the same (let's say "x").
Mubashir

[PICTURE]

In the above picture, spider coordinates are "H3" and fly coordinates are "E2".
The spider will follow the shortest path "H3-H2-H1-A0-E1-E2" to reach the fly.

Examples
spider_vs_fly("H3", "E2") ➞ "H3-H2-H1-A0-E1-E2"

spider_vs_fly("A4", "B2") ➞ "A4-A3-A2-B2"

spider_vs_fly("A4", "C2") ➞ "A4-A3-A2-B2-C2"
Notes
The center of the web will always be A0.
"""

import numpy as np

# Radial ordinal genral coordinates
COORDINATES_RAD = ["0", "1", "2", "3", "4"]
# Circumferential ordinal genral coordinates
COORDINATES_CIR = ["A", "B", "C", "D", "E", "F", "G", "H"]


def determine_adjacent_points(coor_cir, coor_rad, coordinates_rad, coordinates_cir):
    """
    Generator returns tuples:
        - with adjacent coordinates string at first place
        - with distance between 'the_point' and each adjacent 
    coor_cir - circumferential character describing position the point on net
    coor_rad - radial character describing position the point on net
    coordinates_rad - list of radial coordinates in range of interest
    coordinates_cir - list of circumferential coordinates in range of interest
    """
    # Define coordinate string point to be considered throughout generator
    the_point = coor_cir + coor_rad
    # Length between circumferential points 
    circumferential_distance = round(
        np.sqrt(2 * int(coor_rad) ** 2 * (1 - np.cos(45.0 * np.pi / 180))), 2
    )
    # If the_point isn't 0 define each adjacent with distance between them, and
    # each adjacent
    if coor_rad != "0":
        #
        # Coordinate one step counterclockwise:
        index_ccw = coordinates_cir.index(coor_cir) - 1
        # If prospective index is negative do nothing, because it may provide
        # for example that H3 is next to E3 what is faulty!
        if index_ccw >= 0:
            # Create prospective coordinate string point
            add_point = coordinates_cir[index_ccw] + coor_rad
            if add_point != the_point:
                # Yield tuple (coordinate_string, circumferential distance)
                yield (add_point, circumferential_distance)
        #
        # Coordinate with on step clockwise:
        index_cw = coordinates_cir.index(coor_cir) + 1
        # If prospective index is bigger than length do nothing, because it's
        # out of range of interest
        if index_cw < len(coordinates_cir):
            # Create prospective coordinate string point
            add_point = coordinates_cir[index_cw] + coor_rad
            if add_point != the_point:
                # Yield tuple (coordinate_string, circumferential distance)
                yield (add_point, circumferential_distance)
        #
        # Coordinate with on step toward center:
        index_toward = coordinates_rad.index(coor_rad) - 1
        # If prospective index is negative do nothing, because it may provide
        # outward directed point
        if index_toward >= 0:
            # Create prospective coordinate string point
            add_point = coor_cir + coordinates_rad[index_toward]
            if add_point != the_point:
                # If prospective string point has 0, change whole name to 'A0'               
                if add_point[1] == "0":
                    add_point = "A0"
                # Yield tuple (coordinate_string, radial distance)
                yield (add_point, 1)
        #
        # Coordinate with on step outward center:
        index_outward = coordinates_rad.index(coor_rad) + 1
        # If prospective index is bigger than length do nothing, because it's
        # out of range of interest        
        if index_outward < len(coordinates_rad):
            # Create prospective coordinate string point
            add_point = coor_cir + coordinates_rad[index_outward]
            if add_point != the_point:
                # Yield tuple (coordinate_string, radial distance)
                yield (add_point, 1)
    #
    # Center coordinate has special neighbouring points because each one lie 
    # in the radius=1, and only center has max. 8 neighbouring points
    else:
        for cir in coordinates_cir:
            yield (cir + "1", 1)

def limit_map_coordinates(start, end):
    """
    Function return two lists limited to only relevan ranges of the net, based
    on 'start' and 'end' points.
    Returned lists are limited COORDINATES_CIR and COORDINATES_RAD
    start - string describing starting point on the net e.g.: 'A4'
    end - string describing starting point on the net e.g.: 'H1'
    """
    # Define circumferential relevant limits
    range_cir = sorted([start[0], end[0]])
    # Double COORDINATES_CIR to take into account periodicity of the net
    double_coordinates_cir = COORDINATES_CIR * 2
    # Define 3 prospective limits of circumferential relevant coordinates
    # First occurence of start coordinate value
    limit_1 = double_coordinates_cir.index(range_cir[0])
    # First occurence of end coordinate value
    limit_2 = double_coordinates_cir.index(range_cir[-1])
    # Second occurence of start coordinate value (after first occurence of 
    # end coordinate value)
    limit_3 = double_coordinates_cir.index(
        range_cir[0], double_coordinates_cir.index(range_cir[-1])
    )
    # Define 2 prospective slices of circumferential coordinates
    range_1 = double_coordinates_cir[limit_1 : limit_2 + 1]
    range_2 = double_coordinates_cir[limit_2 : limit_3 + 1]
    # Find less length range as more relevant because include shortest nearest
    # path from the start to the end
    coordinates_cir = min([range_1, range_2], key=lambda x: len(x))
    # Define maximal radial point
    range_rad = max([start[-1], end[-1]])
    # Radial coordinates from 0 to maximal radial (it's unappropriate to limit 
    # net radially from center, so abovemention is only condition)
    coordinates_rad = COORDINATES_RAD[0 : COORDINATES_RAD.index(range_rad) + 1]
    # Return slice of the net
    return coordinates_rad, coordinates_cir

def spider_vs_fly(coor_spider, coor_fly):
    """
    Function returns string with consecutive points providing the shortest path
    between spider and fly
    """
    # Limit whole net to the smallest circle sector, includint the shortest path
    coordinates_rad, coordinates_cir = limit_map_coordinates(coor_spider, coor_fly)
    # Create list with each name of all relevant points
    coordinates = [
        coor_cir + coor_rad
        for coor_rad in coordinates_rad
        for coor_cir in coordinates_cir
    ]
    # Remove redundant zeros coordinates like B0, C0 etc.
    coordinates = list(filter(lambda x: "0" not in x, coordinates))
    if "0" in coordinates_rad:
        coordinates.insert(0, "A0")
    # Generate dict of coordinates with adjacent points and their distance
    coordinates_map = {
        coordinate: dict(
            determine_adjacent_points(
                coordinate[0], coordinate[1], coordinates_rad, coordinates_cir
            )
        )
        for coordinate in coordinates
    }
    # Use Dijkstra algorithm to find the shortest path
    dijkstra_tab, path = dijkstra_algorithm(coordinates_map, coor_spider, coor_fly)
    # Return string with consecutive points from spider to fly
    return "-".join(path[::-1])


def distance_start_to_point(dijkstra_tab, start, point):
    """
    Return sum_of_distance between given point and start point based on 
    dijkstra_tab
    """
    # Variable for summing the distance between given point to start point
    sum_of_distance = 0
    # Container for consecutive points between given point to start point
    consecutive_points = []
    # Iterate when next point isn't equal to start, and is not None
    while (point != start) and (not point is None):
        # Append point string to list
        consecutive_points.append(point)
        # Add distance between points 
        sum_of_distance += dijkstra_tab[point]["dv"]
        # Overwrite searching point until it's not start
        point = dijkstra_tab[point]["pv"]
    # Append point start string to list
    consecutive_points.append(start)
    # Return sum_of_distance and list of points
    return sum_of_distance, consecutive_points


def dijkstra_algorithm(coordinates_map, start, end):
    """
    Function returns filled up djikstra's algorithm table and the sortest path
    between start and end point.
    """
    # Create Dijkstra algorithm table with initial point weight 0 and other inf
    dijkstra_tab = {
        point: {
            "dv": np.inf,
            "pv": None,
            "considered": False,
            "to_consideration": False,
        }
        for point in list(coordinates_map.keys())
    }
    # Overwrite values at start point -> assume as first to be considered, and
    # as start point has 0 distance from previous point (because it's not exist)
    dijkstra_tab[start]["dv"] = 0
    dijkstra_tab[start]["to_consideration"] = True
    # Overwrite values at end point as -> exclude straightway as considered
    dijkstra_tab[end]["considered"] = True
    # Iterate indefinite number times until there are next points to consider
    # as prospectively relevant throughout the path.
    next_points_to_consideration = [start]
    while next_points_to_consideration: 
        # Iterate over points to consider as relevant throught the path
        for point_to_consider in next_points_to_consideration:
            # Extract points names adjacent to considered one
            connected_points = coordinates_map[point_to_consider].keys()
            # Iterate over adjacent points
            for connected_point in connected_points:
                # Extract distance between considered point and adjacent (connected)
                add_distance = coordinates_map[point_to_consider][connected_point]
                # Sum whole distance from start until considered one
                distance, _ = distance_start_to_point(
                    dijkstra_tab, start, point_to_consider
                )
                # Increase distance over adjacent point
                distance += add_distance
                # Sum whole distance from start until adjacent (currently saved
                # in table - it's inf initially)
                current_min_distance, _ = distance_start_to_point(
                    dijkstra_tab, start, connected_point
                )
                # If distance between start point and adjacent one is less than
                # currently saved in table - overwrite it as shorter 
                if distance < current_min_distance:  
                    dijkstra_tab[connected_point]["dv"] = add_distance
                    dijkstra_tab[connected_point]["pv"] = point_to_consider
                #
                # If adjacent point hasn't been considered yet, change his 
                # status to_consideration in order to considered it in next
                # iteration
                if dijkstra_tab[connected_point]["considered"] == False:
                    dijkstra_tab[connected_point]["to_consideration"] = True
            # Mark point as considered already (i.e.:considered status states
            # that each adjacent point is checked looking from it, but 
            # to_consideration mean consideration adjacent as considered)
            dijkstra_tab[point_to_consider]["considered"] = True
            dijkstra_tab[point_to_consider]["to_consideration"] = False
                
        # Extract points haven't been considered yet, and connected with previous
        next_points_to_consideration = filter(
            lambda x: x[1]["to_consideration"], dijkstra_tab.items()
        )
        next_points_to_consideration = dict(next_points_to_consideration)
        next_points_to_consideration = list(next_points_to_consideration.keys())
    # Calculate distance and path between start and end based on filled uop
    # dijkstra table
    distance, path = distance_start_to_point(dijkstra_tab, start, end)
    return dijkstra_tab, path


if __name__ == "__main__":

    # aa = spider_vs_fly("A4", "C4")
    # print(aa)
    #
    # bb = spider_vs_fly("A0", "A4")
    # print(bb)
    #
    # cc = spider_vs_fly("A0", "C3")
    # print(cc)
    #
    # dd = spider_vs_fly("C0", "F3")
    # print(dd)
    # # # # # #
    # ee = spider_vs_fly("A1", "B2")
    # print(ee)
    # eee = dijkstra_algorithm(ee, "A1", "B2")
    # print(eee)
    # # # # # #
    # ff = spider_vs_fly("A0", "B2")
    # print(ff)
    # fff = dijkstra_algorithm(ff, "A0", "B2")
    # print(fff)
    ###
    # a = spider_vs_fly("H3", "E2") # ➞ "H3-H2-H1-A0-E1-E2"
    # aa = dijkstra_algorithm(a, "H3", "E2")
    ###
    # a = spider_vs_fly("A4", "B2") # ➞ "A4-A3-A2-B2"
    # aa = dijkstra_algorithm(a, "A4", "B2")
    ###
    # a = spider_vs_fly("A4", "C2") # ➞ "A4-A3-A2-B2-C2"
    # aa = dijkstra_algorithm(a, "A4", "C2")
    ###
    # a = spider_vs_fly("A4", "C1")  #, "A4-A3-A2-A1-B1-C1")
    # aa = dijkstra_algorithm(a, "A4", "C1")

    ### Edabit tests
    import numpy as np

    np.testing.assert_equal(spider_vs_fly("A4", "B1"), "A4-A3-A2-A1-B1")
    np.testing.assert_equal(spider_vs_fly("A4", "C1"), "A4-A3-A2-A1-B1-C1")
    np.testing.assert_equal(spider_vs_fly("A4", "D1"), "A4-A3-A2-A1-A0-D1")
    np.testing.assert_equal(spider_vs_fly("D3", "G3"), "D3-D2-D1-A0-G1-G2-G3")
    np.testing.assert_equal(spider_vs_fly("B2", "E2"), "B2-B1-A0-E1-E2")
    np.testing.assert_equal(spider_vs_fly("H1", "C1"), "H1-A0-C1")
    np.testing.assert_equal(spider_vs_fly("A4", "B2"), "A4-A3-A2-B2")
    np.testing.assert_equal(spider_vs_fly("A4", "C2"), "A4-A3-A2-B2-C2")
    np.testing.assert_equal(spider_vs_fly("A4", "D2"), "A4-A3-A2-A1-A0-D1-D2")
    np.testing.assert_equal(spider_vs_fly("E4", "H4"), "E4-E3-E2-E1-A0-H1-H2-H3-H4")
    np.testing.assert_equal(spider_vs_fly("E4", "G4"), "E4-F4-G4")
    np.testing.assert_equal(spider_vs_fly("E4", "F4"), "E4-F4")