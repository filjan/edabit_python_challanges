"""
https://edabit.com/challenge/XTXZRmvXbhmhSfiPf
Find the Single Number
The function is given a list of numbers where each number appears three times
except for one which appears only one time. Find the single number and return it.

Examples
single_number([2, 2, 3, 2]) ➞ 3

single_number([0, 1, 0, 1, 0, 1, 99]) ➞ 99

single_number([-1, 2, -4, 20, -1, 2, -4, -4, 2, -1]) ➞ 20
Notes
To run under 12 seconds the function needs to be efficient.
"""

def single_number(list_of_numbers):
    """ Function returns value which occured only one time in arg """
    # Find unique values in arg
    uniques = set(list_of_numbers)
    # Count occcurences of each value
    occurences = dict(map(lambda x: (list_of_numbers.count(x), x), uniques))
    # Take value which occured only one time
    return occurences[1]

if __name__ == "__main__":

    from time import perf_counter
    from random import randint, shuffle
    import numpy as np
    tic = perf_counter()

    np.testing.assert_equal(single_number([2, 2, 3, 2]), 3)
    np.testing.assert_equal(single_number([0, 1, 0, 1, 0, 1, 99]), 99)
    np.testing.assert_equal(single_number([-1, 2, -4, 20, -1, 2, -4, -4, 2, -1]), 20)

    t_function = 0
    for _ in range(200):
        set_nums = {randint(-10000, 10000) for _ in range(randint(100, 1000))}
        expected = set_nums.pop()
        lst = sum([[x] * 3 for x in set_nums], [])
        shuffle(lst)
        lst.append(expected)
        shuffle(lst)
        tic_f = perf_counter()
        np.testing.assert_equal(single_number(lst), expected)
        t_function += perf_counter() - tic_f

    print('t_function = {:.3f}'.format(t_function))
    print('       t_total = {:.3f}'.format(perf_counter() - tic))