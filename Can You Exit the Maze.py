"""
https://edabit.com/challenge/PzyssSgqopkBjzTY2
Can You Exit the Maze?
A maze can be represented by a 2D matrix, where 0s represent walkeable areas,
and 1s represent walls. You start on the upper left corner and the exit is on
the most lower right cell.

Create a function that returns true if you can walk from one end of the maze
to the other. You can only move up, down, left and right. You cannot move
diagonally.

Examples
can_exit([
  [0, 1, 1, 1, 1, 1, 1],
  [0, 0, 1, 1, 0, 1, 1],
  [1, 0, 0, 0, 0, 1, 1],
  [1, 1, 1, 1, 0, 0, 1],
  [1, 1, 1, 1, 1, 0, 0]
]) ➞ true

can_exit([
  [0, 1, 1, 1, 1, 1, 1],
  [0, 0, 1, 0, 0, 1, 1],
  [1, 0, 0, 0, 0, 1, 1],
  [1, 1, 0, 1, 0, 0, 1],
  [1, 1, 0, 0, 1, 1, 1]
]) ➞ false

# This maze only has dead ends!

can_exit([
  [0, 1, 1, 1, 1, 0, 0],
  [0, 0, 0, 0, 1, 0, 0],
  [1, 1, 1, 0, 0, 0, 0],
  [1, 1, 1, 1, 1, 1, 0],
  [1, 1, 1, 1, 1, 1, 1]
]) ➞ false

# Exit only one block away, but unreachable!

can_exit([
  [0, 1, 1, 1, 1, 0, 0],
  [0, 0, 0, 0, 1, 0, 0],
  [1, 1, 1, 0, 0, 0, 0],
  [1, 0, 0, 0, 1, 1, 0],
  [1, 1, 1, 1, 1, 1, 0]
]) ➞ true
Notes
In a maze of size m x n, you enter at [0, 0] and exit at [m-1, n-1].
There can be dead ends in a maze - one exit path is sufficient.
"""

from copy import deepcopy

def get_default(arg_list, x, y):
    """
    Function returns None in two specific cases when calling coordinate is
    negative (physically it' position out of the maze)
    """
    # Searching indices must be positive. Return None out of the maze.
    if x < 0 or y < 0:
        value = None
    else:
        # If Searching index is out of the range, return None
        try:
            value = arg_list[x][y]
        except IndexError:
            value = None
    return value

def can_exit(maze, cursor_x=0, cursor_y=0):
    """
    Function checks if it's possiblke to reach right-down corner, starting
    from left-top.
    Function uses recurency to fill whole table with '2' integers, everywhere
    it's possible looking from left-top position'
    """
    # Make copy for comparison at the end of each recur
    maze_copy = deepcopy(maze)
    # Fill starting point with 2
    maze[cursor_y][cursor_x] = 2
    #
    # Go north and recur
    if get_default(maze,cursor_y-1, cursor_x) == 0:
        can_exit(maze, cursor_x, cursor_y-1)
    # Go east and recur
    elif get_default(maze, cursor_y, cursor_x+1) == 0:
        can_exit(maze, cursor_x+1, cursor_y)
    # Go south and recur
    elif get_default(maze, cursor_y+1, cursor_x) == 0:
        can_exit(maze, cursor_x, cursor_y+1)
    # Go west and recur
    elif get_default(maze, cursor_y, cursor_x-1) == 0:
        can_exit(maze, cursor_x-1, cursor_y)
    #
    # If maze has changed with new coordinates, then call again with
    # previous ones
    if maze_copy != maze:
        can_exit(maze, cursor_x, cursor_y)
    # If maze hasn't changed check if the down-right corner is filled with 2
    return bool(maze[len(maze)-1][len(maze[0])-1] == 2)


if __name__ == "__main__":

    import numpy as np

    ## Edabit tests:
    np.testing.assert_equal(can_exit([
     	[0, 1, 1, 1, 1, 1, 1],
     	[0, 0, 1, 1, 0, 1, 1],
     	[1, 0, 0, 0, 0, 1, 1],
     	[1, 1, 1, 1, 0, 0, 1],
     	[1, 1, 1, 1, 1, 0, 0]
    ]), True)

    np.testing.assert_equal(can_exit([
     	[0, 1, 1, 1, 1, 1, 1],
     	[0, 0, 1, 0, 0, 1, 1],
     	[1, 0, 0, 0, 0, 1, 1],
     	[1, 1, 0, 1, 0, 0, 1],
     	[1, 1, 0, 0, 1, 1, 1]
    ]), False)

    np.testing.assert_equal(can_exit([
     	[0, 1, 1, 1, 1, 0, 0],
     	[0, 0, 0, 0, 1, 0, 0],
     	[1, 1, 1, 0, 0, 0, 0],
     	[1, 1, 1, 1, 1, 1, 0],
     	[1, 1, 1, 1, 1, 1, 1]
    ]), False)

    np.testing.assert_equal(can_exit([
     	[0, 1, 1, 1, 1, 0, 0],
     	[0, 0, 0, 0, 1, 0, 0],
     	[1, 1, 1, 0, 0, 0, 0],
     	[1, 0, 0, 0, 1, 1, 0],
     	[1, 1, 1, 1, 1, 1, 0]
    ]), True)

    np.testing.assert_equal(can_exit([
     	[0, 1, 1, 1, 1, 0, 0],
     	[0, 0, 0, 0, 1, 0, 0],
     	[1, 1, 1, 0, 0, 0, 0],
     	[1, 0, 0, 0, 0, 1, 1],
     	[1, 1, 1, 1, 0, 0, 0]
    ]), True)

    np.testing.assert_equal(can_exit([
     	[0, 1, 1, 1, 1, 0, 1],
     	[0, 0, 0, 0, 1, 0, 1],
     	[1, 1, 1, 0, 0, 0, 1],
     	[1, 0, 0, 0, 0, 1, 1],
     	[1, 1, 1, 1, 0, 1, 1]
    ]), False)

    np.testing.assert_equal(can_exit([
     	[0, 0, 0, 0, 0, 0, 0],
     	[0, 0, 0, 0, 1, 0, 0],
     	[1, 1, 1, 0, 0, 0, 0],
     	[1, 0, 0, 0, 0, 1, 0],
     	[1, 1, 1, 1, 0, 1, 0]
    ]), True)

    np.testing.assert_equal(can_exit([
     	[0, 0, 0, 0, 0, 0, 0],
     	[0, 1, 1, 1, 1, 1, 0],
     	[0, 1, 1, 1, 1, 1, 0],
     	[0, 1, 1, 1, 1, 1, 0],
     	[0, 0, 0, 0, 0, 0, 1]
    ]), False)

    np.testing.assert_equal(can_exit([
     	[0, 0, 1, 1, 1, 1, 1],
     	[1, 0, 0, 1, 1, 1, 1],
     	[1, 1, 0, 0, 1, 1, 1],
     	[1, 1, 1, 0, 0, 0, 0],
     	[1, 1, 1, 1, 1, 1, 0]
    ]), True)

    ## More False Tests
    np.testing.assert_equal(can_exit([
     	[0, 0, 0, 0, 0, 0, 0],
     	[0, 1, 1, 1, 1, 1, 0],
     	[0, 1, 1, 1, 1, 1, 0],
     	[0, 1, 1, 1, 1, 1, 1],
     	[0, 0, 0, 0, 0, 1, 0]
    ]), False)

    np.testing.assert_equal(can_exit([
     	[0, 1, 0, 0, 0, 0, 0],
     	[0, 1, 1, 1, 1, 1, 0],
     	[0, 1, 1, 1, 1, 1, 0],
     	[0, 1, 1, 1, 1, 1, 0],
     	[0, 0, 0, 0, 0, 1, 0]
    ]), False)
    